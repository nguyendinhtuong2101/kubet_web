<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title></title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

<script>
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
</script>
<style>
.modal-header
 {
     padding:9px 15px;
     border-bottom:1px solid #eee;
     background-color: #eee;
 }
 body {
	background-color:#f0eff5;
	background-repeat: no-repeat;
	background-size: cover;
	  position: relative;
}
.white{
		background-color:#fff;
		text-align: left;
}
.column {
  float: left;
  padding: 10px;
  height: 60px; /* Should be removed. Only for demonstration */
}
.left {
  width: 25%;
}
.right {
  width: 75%;
}.container{
	max-width: 80%;
	height: 100%;
}
.form-control {
    border: none;
}
.heighttext{
	  border: none;
      display:inline-block;
     min-width: 60%;
     height:100px;
     font-size: 32px;
}
.form-group{
	height: 200px;
  align-items: center;
}
input,
input::-webkit-input-placeholder {
    font-size: 40px;
    line-height: 3;
}



</style>
</head>
<body>

	<script>
			$( document ).ready(function() {
				document.getElementById("btn_registry").disabled = true;
			});
				jQuery(function($) {
						const isEmpty = str => !str.trim().length;
						$('#phone').on('input', function(e){
							if( isEmpty(this.value) ) {
								document.getElementById("btn_login").disabled = true;
								document.getElementById('btn_login').style.backgroundColor = '#aaa';
							} else {
								document.getElementById("btn_login").disabled = false;
								document.getElementById('btn_login').style.backgroundColor = '#ff7500';
							}
						});

					});

	</script>


<div class="container" >
			<form action="insert_regis.php" method="post"
			oninput='password.setCustomValidity(password.value != re_password.value ? "Password not matching." : "")'>

				<div class="row d-flex justify-content-center" style="background-color:#fff; margin-top:70px;align-items: center;	height: 200px;" >
					<img src="http://13.212.210.199/asset/image/icon_join_addPhone.svg" alt="" width="50" height="70">
					<h1 style="margin-top:10px; font-size: 50px"><b>Xác nhận đăng ký</b></h1>
				</div>
				<div > &nbsp&nbsp&nbsp&nbsp	</div>
				<div class="form-group row white">
					 <label class="col-sm-5 col-form-label col-form-label-lg" style="font-size: 45px;">SDT</label>
					 <div class="col-sm-7">
							 <input id="phone" class="heighttext"  name="phone"  type="text" class="form-control form-control-lg" placeholder="10 chữ số" required>
					 </div>
			 </div>
			 <div > &nbsp&nbsp&nbsp&nbsp	</div>
			 <div class="form-group row white">
					<label class="col-sm-5 col-form-label col-form-label-lg"style="font-size: 45px;">Họ và Tên</label>
					<div class="col-sm-7">
							<input type="text" id="fullname" class="heighttext" name="fullname"  class="form-control form-control-lg" placeholder="Nhập họ và tên" required>
					</div>
			</div>
			 <div > &nbsp&nbsp&nbsp&nbsp	</div>
			 <div class="form-group row white">
					<label class="col-sm-5 col-form-label col-form-label-lg"style="font-size: 45px;">Tên đăng nhập</label>
					<div class="col-sm-7">
							<input type="text" id="username" class="heighttext" name="username"  class="form-control form-control-lg" placeholder="Bao gồm cả chữ và số" required>
					</div>
			</div>
			<div > &nbsp&nbsp&nbsp&nbsp	</div>
			<div class="form-group row white">
				 <label class="col-sm-5 col-form-label col-form-label-lg"style="font-size: 45px;">Mật khẩu</label>
				 <div class="col-sm-7">
						 <input type="password" id="password" class="heighttext" name="password" class="form-control form-control-lg" placeholder="6 - 10 ký tự và chữ số"   pattern="^(?=.*[a-zA-Z])(?=\w*[0-9])\w{6,10}$" required>
				 </div>
		 </div>
		 <div > &nbsp&nbsp&nbsp&nbsp	</div>
			<div class="form-group row white">
				 <label class="col-sm-5 col-form-label col-form-label-lg"style="font-size: 45px;">Xác nhận mật khẩu</label>
				 <div class="col-sm-7">
						 <input type="password" id="re_password"  class="heighttext" name="re_password" class="form-control form-control-lg" placeholder="6 - 10 ký tự và chữ số"  pattern="^(?=.*[a-zA-Z])(?=\w*[0-9])\w{6,10}$" required>
				 </div>
		 </div>

		 <div class="row">
        <div class="col">
            <div class="mx-auto w-50 p-3   text-center" style="margin-top: 30px">
							<button type="submit"  id="btn_login" class="btn btn-warning" style=" background-color:#aaa; color:#fff ; width:300px; height:130px;font-size: 50px;"><b>Xác nhận</b></button>
            </div>
        </div>
    </div>

		</form>
</div>
>

</body>
</html>
