<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title></title>
	<meta name="description" content="">

	<!--iOS -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/main.css">
	<script src="js/vendor/modernizr-2.6.2.min.js"></script>
	<style type="text/css">
	.back-link a {
		color: #4ca340;
		text-decoration: none;
		border-bottom: 1px #4ca340 solid;
	}
	.back-link a:hover,
	.back-link a:focus {
		color: #408536;
		text-decoration: none;
		border-bottom: 1px #408536 solid;
	}
	h1 {
		height: 100%;
		/* The html and body elements cannot have any padding or margin. */
		margin: 0;
		font-size: 14px;
		font-family: 'Open Sans', sans-serif;
		font-size: 32px;
		margin-bottom: 3px;
	}
	.entry-header {
		text-align: left;
		margin: 0 auto 50px auto;
		width: 80%;
        max-width: 978px;
		position: relative;
		z-index: 10001;
	}
	/*#demo-content {
		padding-top: 100px;
	}*/
	.hero-image {
  background-image: url("http://13.212.210.199/asset/image/bg_login.png");
  height: 666px;
   background-position: center;

  background-repeat: no-repeat;
  background-size: cover;
}

	</style>
</head>
<body class="demo">
	<!-- Demo content -->
	<div id="demo-content"  class="hero-image">
	<div class="bg-demo">
		<img src="./images/bg_dk.png" alt="">
	</div>
	<div id="loader-wrapper">
		<div id="loader"></div>
		<div  style="text-align:center;color: #ececeb; font-size: 25px;width:100%;margin-top: 250px;" >
			<i>Đường truyền không ổn định vui lòng chờ đợi chúng tôi chuyển đường truyền.</i></div>
		</div>
		<div class="loader-section section-left">	</div>
		<div class="loader-section section-right"></div>
		<div id="content">
			<script>
					 setTimeout(function(){
							 window.location.href="http://ff3259.ku11.net"; // The URL that will be redirected too.
					 }, 3000); // The bigger the number the longer the delay.
			 </script>

		</div>
	</div>
	<!-- /Demo content -->

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
	<script src="js/main.js"></script>

</body>
</html>
