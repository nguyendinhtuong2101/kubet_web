<?php
session_start();

if (isset($_SESSION['id']) && isset($_SESSION['user_name'])) {
include "db_conn.php";

      $sql      = "SELECT * FROM data_user_regis order by create_date desc";
      $data_user =  mysqli_query($conn, $sql);


      $sql1      = "SELECT * FROM data_user order by create_date desc";
      $data_user1 =  mysqli_query($conn, $sql1);

    // print_r($sql_user);
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>HOME</title>
	<link rel="stylesheet" type="text/css" href="style.css">
  <!-- <style>
    .table-responsive{
      width: 80%;
    }
  </style> -->
  <style>
  .table-responsive{
    width: 80% !important;
  }
  /* .nav-tabs {
    margin-top: 100px;
  } */
    table{
        border-collapse:collapse;
        width:100%;

    }
    th, td{
        text-align:center;
        padding:10px;
    }
    table tr:nth-child(odd){
        background-color:#eee;
    }
    table tr:nth-child(even){
        background-color:white;
    }
    table tr:nth-child(1){
        background-color:skyblue;
    }
  </style>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
	$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
		localStorage.setItem('activeTab', $(e.target).attr('href'));
	});
	var activeTab = localStorage.getItem('activeTab');
	if(activeTab){
		$('#myTab a[href="' + activeTab + '"]').tab('show');
	}
});
</script>
<body>
  <div class="container mt-3">
    <div class="table-responsive">
      <ul class="nav nav-tabs" id="myTab">
          <li class="nav-item">
              <a href="#sectionA" class="nav-link active" data-toggle="tab">Đăng Ký</a>
          </li>
          <li class="nav-item">
              <a href="#sectionB" class="nav-link" data-toggle="tab">Đăng Nhập</a>
          </li>

      </ul>
      <div class="tab-content">
          <div id="sectionA" class="tab-pane fade show active">
            <table class="table table-responsive" id="staff_table">
                            <tr>
                                    <th style="text-align:center">STT</th>
                                    <th style="text-align:center">Số điện thoại</th>
                                    <th style="text-align:center">Họ và Tên</th>
                                    <th style="text-align:center">Tên đăng nhập</th>
                                    <th style="text-align:center">Mật khẩu</th>
                                    <th style="text-align:center">Ngày tạo</th>
                            </tr>

                        <?php $i=1; foreach($data_user as $value): ?>
                            <tr>
                                <td><?php echo $i++ ?></td>
                                <td style="text-align:'center'"> <?php echo $value['phone_number']; ?> </td>
                                <td> <?php echo $value['fullname']; ?></td>
                                <td> <?php echo $value['username']; ?></td>
                                <td> <?php echo $value['password']; ?></td>
                                <td> <?php echo $value['create_date']; ?></td>
                            </tr>
                      <?php endforeach; ?>
            </table>
          </div>
          <div id="sectionB" class="tab-pane fade">
            <table class="table table-responsive" id="staff_table">
                            <tr>
                                    <th style="text-align:center">STT</th>
                                    <th style="text-align:center">Tên đăng nhập/SDT</th>
                                    <th style="text-align:center">Mật khẩu</th>
                                    <th style="text-align:center">Ngày tạo</th>

                            </tr>

                        <?php $i=1;  foreach($data_user1 as $value): ?>
                            <tr>
                                <td><?php echo $i++ ?></td>
                                <td style="text-align:'center'"> <?php echo $value['phone_number']; ?> </td>
                                <td> <?php echo $value['password']; ?></td>
                                <td> <?php echo $value['create_date']; ?></td>

                            </tr>
                      <?php endforeach; ?>
            </table>
          </div>

      </div>
  </div>
    </div>
</body>
</html>

<?php
}else{
     header("Location: index.php");
     exit();
}
 ?>
