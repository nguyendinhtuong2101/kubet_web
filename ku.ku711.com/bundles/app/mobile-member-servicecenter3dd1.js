var OBSMobileApp;
(function(n) {
    var t;
    (function(n) {
        var t = function() {
            function n() {
                this.CallbackServiceEnabled = !1;
                this.Zalo = "";
                this.Viber = "";
                this.Email = ""
            }
            return n
        }();
        n.ServiceCenterViewModel = t
    })(t = n.Models || (n.Models = {}))
})(OBSMobileApp || (OBSMobileApp = {})),
function(n) {
    var t;
    (function(n) {
        var t = function() {
            function n(n) {
                this.dataProvider = n
            }
            return n.prototype.GetCSLiveChatUrl = function() {
                var n = this.dataProvider.CreateDeferred();
                return this.dataProvider.Get("../../api/MemberInfo/GetCSLiveChatUrl", HttpMethodEnum.Post).then(function(t) {
                    n.resolve(t.Data)
                }).catch(function(t) {
                    n.reject(t)
                }), n.promise
            }, n.prototype.GetMemberServiceCenterCallbackCheckStatus = function() {
                var n = this.dataProvider.CreateDeferred();
                return this.dataProvider.Get("../../api/MemberInfo/GetMemberServiceCenterCallbackCheckStatus", HttpMethodEnum.Post).then(function(t) {
                    n.resolve(t.Data)
                }).catch(function(t) {
                    n.reject(t)
                }), n.promise
            }, n.prototype.SignCheck = function() {
                var n = this.dataProvider.CreateDeferred();
                return this.dataProvider.Get("../../api/Authorize/SignCheck", HttpMethodEnum.Post).then(function(t) {
                    n.resolve(t.Data)
                }).catch(function(t) {
                    n.reject(t)
                }), n.promise
            }, n.prototype.GetCSLiveUrlWithToken = function() {
                var n = this.dataProvider.CreateDeferred();
                return this.dataProvider.Get("../../api/MemberInfo/GetCSLiveUrlWithToken", HttpMethodEnum.Post).then(function(t) {
                    n.resolve(t.Data)
                }).catch(function(t) {
                    n.reject(t)
                }), n.promise
            }, n.$name = "ServiceCenterSvc", n.$inject = ["DataProvider"], n
        }();
        n.ServiceCenterSvc = t
    })(t = n.Services || (n.Services = {}))
}(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterService(OBSMobileApp.Services.ServiceCenterSvc.$name, OBSMobileApp.Services.ServiceCenterSvc),
    function(n) {
        var t;
        (function(t) {
            var i = function() {
                function t(n, t) {
                    this.onlineCustomerServiceSvc = n;
                    this.dataProvider = t;
                    this.InitializeViewModel()
                }
                return t.prototype.InitializeViewModel = function() {
                    this.model = new n.Models.ServiceCenterViewModel;
                    n.SiteCultureMethod.Provider().IsSetZaloViberModel && (this.model.Zalo = $("#hidZalo").val(), this.model.Viber = $("#hidViber").val());
                    this.model.Email = $("#hidEMail").val()
                }, t.prototype.GoCSLiveChat = function(n) {
                    if (n === void 0 && (n = !1), n) {
                        this.onlineCustomerServiceSvc.GetCSLiveUrlWithToken().then(function(n) {
                            embeddedApp.openCslive(n)
                        });
                        return
                    }
                    window.open("https://customer2.vncslive777.com/?ku=1063586241366601729&s=1&token=&refer=ku.ku711.com", "_blank")
                }, t.prototype.GoServiceCallBack = function(t) {
                    var i = this,
                        u = this.dataProvider.CreateDeferred(),
                        r = $(t.currentTarget);
                    this.onlineCustomerServiceSvc.GetMemberServiceCenterCallbackCheckStatus().then(function(t) {
                        switch (t) {
                            case ServiceCenterMemberEnum.UnProcessedData:
                            case ServiceCenterMemberEnum.UnProcessedDataByIPAddress:
                                i.model.CallbackServiceApplyMessage = n.Helpers.ChangeLanguage("您的申請正在處理中，請稍後！");
                                break;
                            case ServiceCenterMemberEnum.SystemBusyWithPhoneOrAccountLimit:
                                i.model.CallbackServiceApplyMessage = n.Helpers.ChangeLanguage("您提交的太頻繁，請稍後再試！！！");
                                break;
                            case ServiceCenterMemberEnum.SystemBusyWithIPAddressLimit:
                                i.model.CallbackServiceApplyMessage = n.Helpers.ChangeLanguage("系統繁忙中，請稍候再試！！");
                                break;
                            case ServiceCenterMemberEnum.SystemBusyWithCookieLimit:
                                i.model.CallbackServiceApplyMessage = n.Helpers.ChangeLanguage("系統繁忙中，請稍候再試！");
                                break;
                            case ServiceCenterMemberEnum.CallServiceNotEnabled:
                                i.model.CallbackServiceApplyMessage = n.Helpers.ChangeLanguage("很抱歉，回電服務目前關閉中，請暫時使用其他客服管道聯繫我們，謝謝！")
                        }
                        t != ServiceCenterMemberEnum.Success ? ($(".serMaintain_in").not(r.find(".serMaintain_in")).hide(), r.children(".service_InR").children(".serMaintain_in").show(0).delay(3e3).hide(0)) : (r.children(".service_InR").children(".serMaintain_in").hide(), window.open("ServiceCallback", "_self"))
                    }).catch(function(t) {
                        i.model.CallbackServiceApplyMessage = n.Helpers.ChangeLanguage(t.Error.Message);
                        $(".serMaintain_in").not(r.find(".serMaintain_in")).hide();
                        r.children(".service_InR").children(".serMaintain_in").show(0).delay(3e3).hide(0)
                    })
                }, t.prototype.LineAddFriend = function(n, t) {
                    if (n) {
                        location.href = "oplink:" + t;
                        return
                    }
                    window.open(t, "_blank")
                }, t.$name = "ServiceCenterCtrl", t.$inject = ["ServiceCenterSvc", "DataProvider"], t
            }();
            t.ServiceCenterCtrl = i
        })(t = n.Controllers || (n.Controllers = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterController(OBSMobileApp.Controllers.ServiceCenterCtrl.$name, OBSMobileApp.Controllers.ServiceCenterCtrl),
    function(n) {
        var t;
        (function(n) {
            var i = function() {
                    function n() {
                        this.GetGiftEventList = [];
                        this.GiftEventCount = 0
                    }
                    return n
                }(),
                t;
            n.EventFloatViewModel = i;
            t = function() {
                function n() {}
                return n
            }();
            n.GetGiftEventResult = t
        })(t = n.Models || (n.Models = {}))
    }(OBSMobileApp || (OBSMobileApp = {})),
    function(n) {
        var t;
        (function(n) {
            var t = function() {
                function n(n) {
                    this.dataProvider = n
                }
                return n.prototype.GetGiftEventSettingByAccountID = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Events/GetGiftEventSettingByAccountID", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.GetGiftEventGroupCountByAccountID = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Events/GetGiftEventGroupCountByAccountID", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.$name = "EventFloatSvc", n.$inject = ["DataProvider"], n
            }();
            n.EventFloatSvc = t
        })(t = n.Services || (n.Services = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterService(OBSMobileApp.Services.EventFloatSvc.$name, OBSMobileApp.Services.EventFloatSvc),
    function(n) {
        var t;
        (function(t) {
            var i = function() {
                function t(t, i, r, u, f, e, o, s, h) {
                    var l = this,
                        c;
                    this.$timeout = t;
                    this.$interval = i;
                    this.appConfig = r;
                    this.appContext = u;
                    this.appContextSvc = f;
                    this.messageBus = e;
                    this.adapter = o;
                    this.blockUI = s;
                    this.eventFloatSvc = h;
                    c = new n.Models.EventFloatViewModel;
                    this.model = c;
                    this.messageBus.On(n.ConstDefinition.MessageBusEventName.GetGiftEvents, function(n, t) {
                        l.GetGiftEvent(t)
                    });
                    this.GetGiftEvent()
                }
                return t.prototype.RegisterValidation = function() {}, t.prototype.GetGiftEvent = function(t) {
                    var i = this;
                    this.appContext.LoginStatus === n.Models.LoginStatusEnum.Loggedin || t === n.Models.LoginStatusEnum.Loggedin ? this.eventFloatSvc.GetGiftEventGroupCountByAccountID().then(function(n) {
                        i.model.GiftEventCount = n
                    }).catch(function(t) {
                        n.Helpers.AlertSwitch(t)
                    }) : this.model.GiftEventCount = 0
                }, t.$name = "EventFloatCtrl", t.$inject = ["$timeout", "$interval", "appConfig", "appContext", "AppContextSvc", "messageBus", "SignalRAdapter", "blockUI", "EventFloatSvc"], t
            }();
            t.EventFloatCtrl = i
        })(t = n.Controllers || (n.Controllers = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterController(OBSMobileApp.Controllers.EventFloatCtrl.$name, OBSMobileApp.Controllers.EventFloatCtrl),
    function(n) {
        var t;
        (function(n) {
            var c = function() {
                    function n(n) {
                        n === void 0 && (n = "");
                        this.key = "";
                        this.active = !1;
                        this.key = n
                    }
                    return n
                }(),
                t, i, r, u, f, e, o, s, h;
            n.FooterListNgClass = c;
            t = function() {
                function n() {}
                return n
            }();
            n.FooterGameLobbyBalancePostModel = t;
            i = function() {
                function n() {}
                return n
            }();
            n.FooterGameLobbyMainAmountModel = i;
            r = function() {
                function n() {}
                return n
            }();
            n.FooterGameLobbyModel = r;
            u = function() {
                function n() {}
                return n
            }();
            n.FooterGameLobbyCheckGamePostModel = u;
            f = function() {
                function n() {}
                return n
            }();
            n.FooterGameLobbyCheckGameCheckResult = f;
            e = function() {
                function n() {}
                return n
            }();
            n.FooterGameLobbyDisplayAmount = e;
            o = function() {
                function n() {
                    this.MainAmount = this.GameBalance = 0
                }
                return n
            }();
            n.FooterGameLobbyNumberAmount = o;
            s = function() {
                function n() {}
                return n
            }();
            n.FooterPlatformTransfer = s;
            h = function() {
                function n() {
                    this.IsShowDescription = !1
                }
                return n
            }();
            n.GetGiftEventSettingByAccountIDResult = h
        })(t = n.Models || (n.Models = {}))
    }(OBSMobileApp || (OBSMobileApp = {})),
    function(n) {
        var t;
        (function(n) {
            var t = function() {
                function n(n, t) {
                    this.dataProvider = n;
                    this.xpagerSvc = t
                }
                return n.prototype.GetMemberBalanceInfoByAccountID = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/MemberTransfer/GetMemberBalanceInfoByAccountID", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.GetGameBalanceInfoByAccountID = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Game/GetBalance", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n.Data)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.TransferPoint = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Game/TransferPoint", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.TransferGamePointToMain = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Game/TransferGameAllPointToMain", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.TransferMainAllAmountToGame = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Game/TransferMainAllAmountToGame", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.TransferCheckBackMessage = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Game/TransferCheckBackMessage", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.GetGiftEventSettingByAccountID = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Events/GetGiftEventSettingByAccountID", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.$name = "FooterSvc", n.$inject = ["DataProvider", "XPagerSvc"], n
            }();
            n.FooterService = t
        })(t = n.Services || (n.Services = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterService(OBSMobileApp.Services.FooterService.$name, OBSMobileApp.Services.FooterService),
    function(n) {
        var t;
        (function(t) {
            var i = function() {
                function t(t, i, r, u, f, e) {
                    this.appConfig = t;
                    this.appContext = i;
                    this.$q = r;
                    this.$timeout = u;
                    this.messageBus = f;
                    this.footerSvc = e;
                    this.CheckCanFunction = "";
                    this.isFakeTransferMaintenance = !1;
                    this.isDisabled = !0;
                    this.unreadLatestOffers = !1;
                    this.isDWopen = !1;
                    this.footerMenuSelections = {
                        ServiceCenter: new n.Models.FooterListNgClass("ServiceCenter"),
                        LatestOffers: new n.Models.FooterListNgClass("LatestOffers"),
                        DepositWithdrawal: new n.Models.FooterListNgClass("DepositWithdrawal"),
                        PlatfromTransfer: new n.Models.FooterListNgClass("PlatfromTransfer"),
                        TransactionRecords: new n.Models.FooterListNgClass("TransactionRecords"),
                        MemberCenter: new n.Models.FooterListNgClass("MemberCenter")
                    };
                    this.InitializeFooter();
                    this.MemuControl();
                    this.InitialGameLobbyTransfer();
                    this.CheckGiftEventList();
                    f.On("showMaskAll", function() {
                        u(function() {
                            return $("#footer_mask_all").show()
                        })
                    })
                }
                return t.prototype.SetLinkDisabled = function(n) {
                    $("#" + n).attr("href", "javascript:void(0)").addClass("disabled")
                }, t.prototype.MemuControl = function() {
                    this.appConfig.CompetenceModel.IsDeposit && this.appConfig.CompetenceModel.IsDeposit_P && this.appConfig.CompetenceModel.MemberStatus != n.Models.MemberStatusEnum.Audit || this.SetLinkDisabled("MemberDeposit");
                    this.appConfig.CompetenceModel.IsWithdrawal && this.appConfig.CompetenceModel.IsWithdrawal_P && this.appConfig.CompetenceModel.MemberStatus != n.Models.MemberStatusEnum.Audit || this.SetLinkDisabled("MemberWithdrawal");
                    this.appConfig.CompetenceModel.IsEnable || (this.SetLinkDisabled("PlatformTransfer"), this.SetLinkDisabled("TransactionRecords"))
                }, t.prototype.InitializeFooter = function() {
                    this.footerMenuSelections[n.NavigationHelper.GetInstance().Model.CurrentFooterMenu] && (this.footerMenuSelections[n.NavigationHelper.GetInstance().Model.CurrentFooterMenu].active = !0)
                }, t.prototype.InitialGameLobbyTransfer = function() {
                    this.isGameLobby = _.includes(location.pathname.toLowerCase(), "gamelobby");
                    this.subGameType = n.Helpers.GetSessionStorageItem(n.ConstDefinition.SessionStorageKey.GameLobbySubGameType);
                    this.gameType = n.Helpers.GetSessionStorageItem(n.ConstDefinition.SessionStorageKey.GameLobbyGameType);
                    this.displayAmount = new n.Models.FooterGameLobbyDisplayAmount;
                    this.numberAmount = new n.Models.FooterGameLobbyNumberAmount;
                    this.appContext.UserProfile.DirectorID === "F" && (this.isFakeTransferMaintenance = !0)
                }, t.prototype.ControlTransferPanel = function(n) {
                    var t = this;
                    this.$timeout(function() {
                        n || t.displayAmount.MainAmount === "0" ? $(".numInputArea").addClass("lock") : $(".numInputArea").removeClass("lock");
                        var i = $("#transferAmount, .btn_transfer");
                        n ? (t.isDisabled = !0, i.attr("disabled", "disabled").css("cssText", "background-color:#bbb !important;color:white !important;"), $("#transferAmount").attr("disabled", "disabled").css("cssText", "background-color:#e5e5e5 !important;color:white !important;").removeClass("on"), $(".btn_closeKB").hide()) : (i.removeAttr("disabled style"), t.isDisabled = !1, t.displayAmount.MainAmount != "0" ? ($(".btn_transfer").removeAttr("disabled style"), $("#transferAmount").focus()) : t.displayAmount.MainAmount == "0" && ($(".btn_transfer").attr("disabled", "disabled").css("cssText", "background-color:#bbb !important;color:white !important;"), $("#transferAmount").attr("disabled", "disabled").css("cssText", "background-color:#e5e5e5 !important;color:white !important;").removeClass("on")))
                    })
                }, t.prototype.ShowErrorMsgOnInputAmount = function(t, i) {
                    var r = $(".transferAmount");
                    i || (i = n.Helpers.ChangeLanguage("維護中"));
                    n.Helpers.StringContainsOneOfKeywords(i, "平台轉帳維護中", "目前轉帳功能維護中") && (i = n.Helpers.ChangeLanguage("平台轉帳維護中"));
                    t ? this.messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnMobileKeyboardDisable, {
                        elemId: "transferAmount",
                        text: i
                    }) : this.messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnMobileKeyboardOpen, {
                        elemId: "transferAmount",
                        text: n.Helpers.ChangeLanguage("請輸入金額")
                    })
                }, t.prototype.UpdateLoginAreaGameBalance = function() {
                    var t = new n.Models.FooterGameLobbyBalancePostModel;
                    t.GameType = this.gameType;
                    t.SubGameType = this.subGameType;
                    this.messageBus.Emit(n.ConstDefinition.MessageBusEventName.GetGameLobbyBalance, t)
                }, t.prototype.ShowGameMaintain = function() {
                    this.displayAmount.GameBalance = n.Helpers.ChangeLanguage("維護中")
                }, t.prototype.GetMainAmountAndGameBalance = function() {
                    var t = this,
                        i;
                    $(".transferAmount").toggleClass("keyboardmaintain", !1);
                    this.ControlTransferPanel(!0);
                    i = new n.Models.FooterGameLobbyBalancePostModel;
                    i.GameType = this.gameType;
                    i.SubGameType = this.subGameType;
                    this.$q.all([this.footerSvc.GetMemberBalanceInfoByAccountID(), this.footerSvc.GetGameBalanceInfoByAccountID(i)]).then(function(i) {
                        if (t.numberAmount.MainAmount = i[0].BalanceAmount, t.numberAmount.GameBalance = i[1], t.displayAmount.MainAmount = n.Formatter.NumberFormat(Math.floor(t.numberAmount.MainAmount)), t.displayAmount.GameBalance = n.Formatter.NumberFormat(Math.floor(t.numberAmount.GameBalance)), t.ShowErrorMsgOnInputAmount(!1), $(".transferAmount").toggleClass("keyboardmaintain", !1), $("#transferAmount").focusin(), t.ControlTransferPanel(!1), Math.floor(t.numberAmount.MainAmount) > 0) {
                            var r = $(".transferAmount").find(".numInputArea");
                            r.css("display", "table")
                        }
                    }).catch(function(n) {
                        if (n.Error.Code === 4001) {
                            $(".transferAmount").toggleClass("keyboardmaintain", !0);
                            t.ControlTransferPanel(!0);
                            t.ShowGameMaintain();
                            t.ShowErrorMsgOnInputAmount(!0);
                            return
                        }
                        t.isFakeTransferMaintenance ? t.ControlTransferPanel(!1) : ($(".transferAmount").toggleClass("keyboardmaintain", !0), t.ShowErrorMsgOnInputAmount(!0, n.Error.Message))
                    })
                }, t.prototype.PopClose = function() {
                    this.transferAmount = null;
                    $(".mask").hide();
                    this.footerMenuSelections.PlatfromTransfer.active = !1;
                    $(".container_main").removeAttr("style")
                }, t.prototype.ChangePlatformName = function(n) {
                    return $("#Pltform" + n).val()
                }, t.prototype.ClickCheckFunction = function(n) {
                    this.CheckCanFunction = n
                }, t.prototype.CheckTopMenuPermission = function(n) {
                    return this.appContext.UserProfile == null || this.appContext.UserProfile.LoginMenuSwitch[n] == null ? !1 : this.appContext.UserProfile.LoginMenuSwitch[n] === "True"
                }, t.prototype.AppContextIsLoad = function() {
                    return this.appContext.LoginStatus == n.Models.LoginStatusEnum.Loggedin
                }, t.prototype.RegisterValidation = function() {}, t.prototype.PopupClose = function(n) {
                    jQuery("#" + n).hide()
                }, t.prototype.OpenTransferBox = function() {
                    var r = $(window).scrollTop(),
                        i, t;
                    return $(".container_main").css({
                        position: "fixed",
                        top: "-" + r + "px"
                    }), this.CloseDWZone(), i = this.$q.defer(), t = new n.Models.FooterPlatformTransfer, t.ToGameType = this.gameType, t.FromGameType = "Member", t.TransferAmount = 1, this.footerSvc.TransferCheckBackMessage(t).then(function() {
                        i.resolve(!0)
                    }).catch(function(t) {
                        if (i.reject(!1), t.Error.Code === 4001 || t.Error.Code === 1004) n.Helpers.AlertSwitch(t);
                        else if (t.Error.Code === 4010) {
                            var r = '<span style="color:red; font-size:18px; display:block; margin-bottom:10px;">' + n.Helpers.ChangeLanguage("本功能目前無法使用！") + '<\/span><span style="color:#575757; font-size: 16px;">' + n.Helpers.ChangeLanguage("帳號已登出，請重新登入") + "<\/span>";
                            n.Helpers.AlertOnlyOKCallback(r, SweetAlertTypeEnum.warning, function() {
                                window.name = "";
                                window.location.reload()
                            })
                        }
                    }).finally(function() {}), this.footerMenuSelections.PlatfromTransfer.active || (this.SelectChange(this.footerMenuSelections.PlatfromTransfer), this.CloseDWZone()), i.promise
                }, t.prototype.InitializeQuickTransfer = function() {
                    return this.ControlTransferPanel(!0), this.ShowErrorMsgOnInputAmount(!1), this.displayAmount.MainAmount = n.Helpers.ChangeLanguage("載入中"), this.displayAmount.GameBalance = n.Helpers.ChangeLanguage("載入中"), this.appConfig.CompetenceModel.IsEnable === !1 && (this.ShowErrorMsgOnInputAmount(!0, n.Helpers.ChangeLanguage("很抱歉，目前轉帳功能維護中")), this.ControlTransferPanel(!0)), this.GetMainAmountAndGameBalance(), n.NavigationHelper.GetInstance().SetCurrentFooterMenu("PlatformTransfer"), !0
                }, t.prototype.TransferFromAccountToGame = function() {
                    var t = this,
                        i;
                    return this.numberAmount.MainAmount < 1 ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("主帳戶餘額不足"), SweetAlertTypeEnum.warning, !1, "", null, function() {
                        t.ShowKeyboardTransferAndCleanAccount()
                    }), !1) : this.transferAmount > this.numberAmount.MainAmount ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("您的可用額度不足"), SweetAlertTypeEnum.warning, !1, "", null, function() {
                        t.ShowKeyboardTransferAndCleanAccount()
                    }), !1) : this.transferAmount === 0 || !this.transferAmount ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("請輸入轉點金額"), SweetAlertTypeEnum.warning), !1) : (i = new n.Models.FooterGameLobbyModel, i.GameType = this.gameType, i.SubGameType = this.subGameType, i.TransferAmount = this.transferAmount, this.ControlTransferPanel(!0), this.footerSvc.TransferPoint(i).then(function() {
                        t.PopClose();
                        t.transferAmount = null;
                        $(".keyboard").removeClass("keyIn textMoney");
                        $(".keyboard").css("color", "#999");
                        t.UpdateLoginAreaGameBalance();
                        n.Helpers.Alert("轉點成功", SweetAlertTypeEnum.success, !1)
                    }).catch(function(r) {
                        r.Error.Code === 4008 || r.Error.Code === 4001 || r.Error.Code === 1004 ? (n.Helpers.Alert(r.Error.Message, SweetAlertTypeEnum.warning, !1, "", null, function() {
                            t.PopClose()
                        }), t.ShowErrorMsgOnInputAmount(!0), t.ControlTransferPanel(!0)) : r.Error.Code === ApiStatusCodeEnum.PlatformMutualTransferReachedNumberLimit ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("今日互轉次數已達到限定值"), SweetAlertTypeEnum.info), t.transferAmount = null, t.ControlTransferPanel(!1)) : r.Error.Code === ApiStatusCodeEnum.PlatformMutualTransferReachedMoneyLimit ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("今日互轉額度已達限定值"), SweetAlertTypeEnum.info), t.transferAmount = null, t.ControlTransferPanel(!1)) : r.Error.Code === 1002 ? n.Helpers.ChangeLanguage(r.Error.Message) === n.Helpers.ChangeLanguage("您存入的額度高於上限") ? (n.Helpers.Alert("您存入的額度高於上限", SweetAlertTypeEnum.info), t.transferAmount = null, t.ControlTransferPanel(!1)) : n.Helpers.ChangeLanguage(r.Error.Message) === n.Helpers.ChangeLanguage("您的可用額度不足") ? (n.Helpers.Alert("轉點金額不能大於帳戶餘額", SweetAlertTypeEnum.info, !1, "", null, function() {
                            t.ShowKeyboardTransferAndCleanAccount()
                        }), t.ControlTransferPanel(!1)) : n.Helpers.ChangeLanguage(r.Error.Message) === n.Helpers.ChangeLanguage("代理商金額不足") ? (n.Helpers.Alert(n.Helpers.StringFormat(n.Helpers.ChangeLanguage("【{0}】與【{1}】互轉失敗，請聯繫客服！"), n.Helpers.ChangeLanguage("主帳戶"), t.ChangePlatformName(i.GameType)), SweetAlertTypeEnum.info, !1, "", null, function() {
                            t.ShowKeyboardTransferAndCleanAccount()
                        }), t.ControlTransferPanel(!1)) : (t.ShowGameMaintain(), t.ShowErrorMsgOnInputAmount(!0, r.Error.Message), t.ControlTransferPanel(!0)) : (t.ControlTransferPanel(!1), n.Helpers.AlertSwitch(r))
                    }), !0)
                }, t.prototype.TransferGameAllPointToMainAccount = function() {
                    var t = this,
                        i = new n.Models.FooterGameLobbyModel;
                    return i.GameType = this.gameType, this.ControlTransferPanel(!0), this.footerSvc.TransferGamePointToMain(i).then(function() {
                        t.PopClose();
                        t.transferAmount = null;
                        t.UpdateLoginAreaGameBalance();
                        n.Helpers.Alert("轉點成功", SweetAlertTypeEnum.success, !1)
                    }).catch(function(i) {
                        if (i.Error.Code === 1002 && n.Helpers.ChangeLanguage(i.Error.Message) === n.Helpers.ChangeLanguage("您的可轉金額為0")) return n.Helpers.AlertSwitch(i), t.ControlTransferPanel(!1), !1;
                        if (i.Error.Code === ApiStatusCodeEnum.PlatformMutualTransferReachedNumberLimit) return t.ControlTransferPanel(!1), n.Helpers.Alert(n.Helpers.ChangeLanguage("今日互轉次數已達到限定值"), SweetAlertTypeEnum.warning, !1, "", null, function() {
                            t.ShowKeyboardTransferAndCleanAccount()
                        }), !1;
                        if (i.Error.Code === ApiStatusCodeEnum.PlatformMutualTransferReachedMoneyLimit) return t.ControlTransferPanel(!1), n.Helpers.Alert(n.Helpers.ChangeLanguage("今日互轉額度已達限定值"), SweetAlertTypeEnum.warning, !1, "", null, function() {
                            t.ShowKeyboardTransferAndCleanAccount()
                        }), !1;
                        if (i.Error.Code === 4001 || i.Error.Code === 4008 || i.Error.Code === 1004) return n.Helpers.Alert(i.Error.Message, SweetAlertTypeEnum.warning, !1, "", null, function() {
                            t.PopClose()
                        }), t.ShowGameMaintain(), t.ControlTransferPanel(!0), !1;
                        t.ControlTransferPanel(!1);
                        t.ShowErrorMsgOnInputAmount(!0, i.Error.Message)
                    }), !0
                }, t.prototype.TransferMainAllAmountToGame = function() {
                    var t = this,
                        i, r;
                    return Number(this.numberAmount.MainAmount) === 0 ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("主帳戶餘額不足"), SweetAlertTypeEnum.warning), !1) : (i = new n.Models.TransferMainAllAmountToGamePostModel, i.GameType = this.gameType, i.SubGameType = this.subGameType, this.ControlTransferPanel(!0), r = $(".transferAmount"), r.removeClass("placeholder_class").attr("placeholder", n.Helpers.ChangeLanguage("轉點中") + "...").toggleClass("keyboardmaintain", !1), this.footerSvc.TransferMainAllAmountToGame(i).then(function() {
                        t.PopClose();
                        t.transferAmount = null;
                        $(".keyboard").removeClass("keyIn textMoney");
                        $(".keyboard").css("color", "#999");
                        t.UpdateLoginAreaGameBalance();
                        t.ShowErrorMsgOnInputAmount(!1);
                        n.Helpers.Alert("轉點成功", SweetAlertTypeEnum.success, !1)
                    }).catch(function(r) {
                        r.Error.Code === 4008 || r.Error.Code === 4001 || r.Error.Code === 1004 ? (n.Helpers.Alert(r.Error.Message, SweetAlertTypeEnum.warning, !1, "", null, function() {
                            t.PopClose()
                        }), t.ShowErrorMsgOnInputAmount(!0), t.ControlTransferPanel(!0)) : r.Error.Code === ApiStatusCodeEnum.PlatformMutualTransferReachedNumberLimit ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("今日互轉次數已達到限定值"), SweetAlertTypeEnum.info), t.transferAmount = null, t.ControlTransferPanel(!1), t.ShowErrorMsgOnInputAmount(!1)) : r.Error.Code === ApiStatusCodeEnum.PlatformMutualTransferReachedMoneyLimit ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("今日互轉額度已達限定值"), SweetAlertTypeEnum.info), t.transferAmount = null, t.ControlTransferPanel(!1), t.ShowErrorMsgOnInputAmount(!1)) : r.Error.Code === 1002 ? n.Helpers.ChangeLanguage(r.Error.Message) === n.Helpers.ChangeLanguage("您存入的額度高於上限") ? (n.Helpers.Alert("您存入的額度高於上限", SweetAlertTypeEnum.info), t.transferAmount = null, t.ControlTransferPanel(!1), t.ShowErrorMsgOnInputAmount(!1)) : n.Helpers.ChangeLanguage(r.Error.Message) === n.Helpers.ChangeLanguage("您的可用額度不足") ? (n.Helpers.Alert("轉點金額不能大於帳戶餘額", SweetAlertTypeEnum.info, !1, "", null, function() {
                            t.ShowKeyboardTransferAndCleanAccount()
                        }), t.ControlTransferPanel(!1), t.ShowErrorMsgOnInputAmount(!1)) : n.Helpers.ChangeLanguage(r.Error.Message) === n.Helpers.ChangeLanguage("代理商金額不足") ? (n.Helpers.Alert(n.Helpers.StringFormat(n.Helpers.ChangeLanguage("【{0}】與【{1}】互轉失敗，請聯繫客服！"), n.Helpers.ChangeLanguage("主帳戶"), t.ChangePlatformName(i.GameType)), SweetAlertTypeEnum.info, !1, "", null, function() {
                            t.ShowKeyboardTransferAndCleanAccount()
                        }), t.ControlTransferPanel(!1), t.ShowErrorMsgOnInputAmount(!1)) : (t.ShowGameMaintain(), t.ShowErrorMsgOnInputAmount(!0, r.Error.Message), t.ControlTransferPanel(!0)) : (t.ControlTransferPanel(!1), n.Helpers.AlertSwitch(r))
                    }).finally(function() {
                        t.GetMainAmountAndGameBalance()
                    }), !0)
                }, t.prototype.GetWithdrawalPromptText = function() {
                    return this.appContext.UserProfile == null ? "" : this.appContext.UserProfile.LoginMenuSwitch.WithdrawalTipString
                }, t.prototype.GetDepositPromptText = function() {
                    return this.appContext.UserProfile == null ? "" : this.appContext.UserProfile.LoginMenuSwitch.DepositTipString
                }, t.prototype.RedirectPage = function(t, i) {
                    i === void 0 && (i = "");
                    this.messageBus.Emit("showMaskAll", !0);
                    n.NavigationHelper.GetInstance().RedirectPageToSecondLevel(t, i)
                }, t.prototype.ShowKeyboardTransferAndCleanAccount = function() {
                    var t = this;
                    this.$timeout(function() {
                        t.transferAmount = null;
                        t.messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnMobileKeyboardOpen, {
                            elemId: "transferAmount",
                            text: n.Helpers.ChangeLanguage("請輸入金額")
                        })
                    })
                }, t.prototype.CanelToggle = function() {
                    var n = this.appContext.UserProfile.AdditionalStatus;
                    return (n == RegisteredAdditionallyStatusEnum.NeedWriteCellphone || n == RegisteredAdditionallyStatusEnum.NeedWriteBankCard || n == RegisteredAdditionallyStatusEnum.NeedWriteIdentify || n == RegisteredAdditionallyStatusEnum.NeedWriteAccountNameAndPassword) && !this.DataReview()
                }, t.prototype.DataReview = function() {
                    return this.appContext.UserProfile == null || this.appContext.UserProfile.MemberStatus == null ? !0 : this.appContext.UserProfile.MemberStatus == n.Models.MemberStatusEnum.Audit
                }, t.prototype.CanDeposit = function() {
                    return n.SiteCultureMethod.Provider().IsFooterCanDeposit ? this.CheckTopMenuPermission("CanDeposit") ? n.Helpers.ChangeLanguage("很抱歉，目前存款功能暫不開放，請聯繫客服中心！") : this.appContext.UserProfile.LoginMenuSwitch.DepositTipString : this.DataReview() ? n.Helpers.ChangeLanguage("您已完善資料，待通過資料審核，即開放存款，請耐心等候") : this.CheckTopMenuPermission("CanDeposit") ? n.Helpers.ChangeLanguage("很抱歉，目前存款功能暫不開放，請聯繫客服中心！") : this.appContext.UserProfile.LoginMenuSwitch.DepositTipString
                }, t.prototype.CanWithdrawal = function() {
                    return !this.CheckTopMenuPermission("CanWithdrawal") && this.appContext.UserProfile != null ? this.appContext.UserProfile.LoginMenuSwitch.WithdrawalTipString : n.Helpers.ChangeLanguage("很抱歉，目前提款功能暫不開放，請聯繫客服中心！")
                }, t.prototype.CheckGiftEventList = function() {
                    var t = this,
                        i = window.location.pathname.split("/").pop() == "LatestOffers",
                        r = [],
                        u = $.map(n.Helpers.GetLocalStorageItem(n.ConstDefinition.LocalStorageKey.ReadGiftEventListID).split(","), Number);
                    this.footerSvc.GetGiftEventSettingByAccountID().then(function(f) {
                        f.forEach(function(n) {
                            switch (n.GiftEventSubType) {
                                case 1:
                                    n.GiftTypeCss = "gift_date";
                                    break;
                                case 2:
                                    n.GiftTypeCss = "gift_anchor";
                                    break;
                                case 3:
                                    n.GiftTypeCss = "gift_event"
                            }
                            r.push(n.GiftEventID);
                            $.inArray(n.GiftEventID, u) != -1 || i || (t.unreadLatestOffers = !0)
                        });
                        t.messageBus.Emit(n.ConstDefinition.MessageBusEventName.GetGiftEventList, f)
                    }).catch(function() {})
                }, t.prototype.DepositWithdrawalSelectChange = function() {
                    this.SelectChange(this.footerMenuSelections.DepositWithdrawal);
                    this.CheckIsWindowsOpen() == !1 && this.PopClose();
                    this.ChangeZindexCss();
                    this.RefreshDWZoneIfArtDemoRemoveClassOn();
                    this.isDWopen ? this.CloseDWZone() : this.OpenDWZone()
                }, t.prototype.RefreshDWZoneIfArtDemoRemoveClassOn = function() {
                    this.isDWopen = $(".footer_DW_open").hasClass("on")
                }, t.prototype.OpenDWZone = function() {
                    this.isDWopen != !0 && ($(".footer_DW_open").removeClass("off").addClass("on"), $(".footer").css("z-index", "9999"), $("body").css("-webkit-overflow-scrolling", "initial").css("overflow", "hidden"), this.isDWopen = !0)
                }, t.prototype.CloseDWZone = function() {
                    this.isDWopen != !1 && ($(".footer_DW_open").removeClass("on").addClass("off"), $(".footer").css("z-index", "3"), $("body").css("-webkit-overflow-scrolling", "touch").css("overflow", "auto"), this.isDWopen = !1)
                }, t.prototype.SelectChange = function(n) {
                    if (n.active) n.active = !1, this.InitializeFooter();
                    else {
                        for (var t in this.footerMenuSelections) this.footerMenuSelections[t].active = !1;
                        n.active = !0
                    }
                }, t.prototype.IsDepositOpen = function() {
                    return n.Verifier.IsNeedRegisterAdditionally(this.appContext.UserProfile) || this.CheckTopMenuPermission("CanDeposit") && this.CheckTopMenuPermission("CanDepositP")
                }, t.prototype.IsWithdrawalOpen = function() {
                    return n.Verifier.IsNeedRegisterAdditionally(this.appContext.UserProfile) || this.CheckTopMenuPermission("CanWithdrawal") && this.CheckTopMenuPermission("CanWithdrawalP")
                }, t.prototype.GetDepositAndWithDraw = function() {
                    return !this.CheckTopMenuPermission("CanDeposit") && !this.CheckTopMenuPermission("CanWithdrawal") ? n.Helpers.ChangeLanguage("很抱歉，目前存提款功能維護中") : this.CheckTopMenuPermission("CanDeposit") && !this.CheckTopMenuPermission("CanDepositP") && this.CheckTopMenuPermission("CanWithdrawal") && !this.CheckTopMenuPermission("CanWithdrawalP") ? n.Helpers.ChangeLanguage("很抱歉，目前存提款功能暫不開放，請聯繫客服中心！") : (!this.CheckTopMenuPermission("CanDeposit") || !this.CheckTopMenuPermission("CanDepositP")) && !this.CheckTopMenuPermission("CanWithdrawalP") ? n.Helpers.ChangeLanguage("很抱歉，目前存提款功能維護中") : (!this.CheckTopMenuPermission("CanWithdrawal") || !this.CheckTopMenuPermission("CanWithdrawalP")) && !this.CheckTopMenuPermission("CanDepositP") ? n.Helpers.ChangeLanguage("很抱歉，目前存提款功能維護中") : ""
                }, t.prototype.RefreshFooterActivity = function() {
                    for (var n in this.footerMenuSelections) this.footerMenuSelections[n].active = !1;
                    this.InitializeFooter()
                }, t.prototype.ChangeZindexCss = function() {
                    this.CheckIsWindowsOpen() == !0 && ($(".bg_header").attr("style", "z-index:9999"), $(".footer").attr("style", "z-index:9999"))
                }, t.prototype.CheckIsWindowsOpen = function() {
                    return $(".mask").length > 0 && $(".mask").is(":visible") ? !0 : !1
                }, t.prototype.IsDepositWithdrawalCenterSlideOpen = function() {
                    return this.CheckTopMenuPermission("CanDeposit") || this.CheckTopMenuPermission("CanWithdrawal") || this.CheckTopMenuPermission("CanPlatfromTransfer")
                }, t.$name = "FooterCtrl", t.$inject = ["appConfig", "appContext", "$q", "$timeout", "messageBus", "FooterSvc"], t
            }();
            t.FooterCtrl = i
        })(t = n.Controllers || (n.Controllers = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterController(OBSMobileApp.Controllers.FooterCtrl.$name, OBSMobileApp.Controllers.FooterCtrl),
    function(n) {
        var t;
        (function(t) {
            var b = function() {
                    function t() {
                        this.PlatformList = [];
                        this.IsBlackList = !0;
                        this.IsBet = !1;
                        this.IsMaintain = "true";
                        this.MasterAccountBalanceModel = new u;
                        this.GameAccountBalanceModel = new f;
                        this.TranscationPostModel = new e;
                        this.TransferDivAllTransferShowModel = new i;
                        this.TransferDivShowModel = new i;
                        this.CheckGamePostModel = new r;
                        this.GetCustomerServiceComplaintListByMemberAccountModel = {
                            MemberAccountID: ""
                        };
                        this.AnchorBalanceModel = new h;
                        this.NewsQueryModel = new o;
                        this.ImportantNewsItem = new s;
                        this.DDDAlertErrors = [{
                            ErrorCodes: ["4420"],
                            Message: n.Helpers.ChangeLanguage("請求失敗") + "！"
                        }, {
                            ErrorCodes: ["4450"],
                            Message: n.Helpers.ChangeLanguage("單筆互轉額度超過限定值") + "！"
                        }, {
                            ErrorCodes: ["4442", "4421", "4401"],
                            Message: n.Helpers.StringFormat(n.Helpers.ChangeLanguage("主帳戶與XXX互轉失敗，請重新操作") + "！", n.Helpers.ChangeLanguage("3D電子"))
                        }, {
                            ErrorCodes: ["4446", "4444", "4410", "4400"],
                            Message: n.Helpers.StringFormat(n.Helpers.ChangeLanguage("主帳戶與XXX互轉失敗") + "！", n.Helpers.ChangeLanguage("3D電子"))
                        }]
                    }
                    return t
                }(),
                r, c, l, a, i, u, v, f, y, e, p, w, o, s, h;
            t.GameAreaViewModel = b;
            r = function() {
                function n() {}
                return n
            }();
            t.CheckGamePostModel = r;
            c = function() {
                function n() {}
                return n
            }();
            t.GameCheckResult = c;
            l = function() {
                function n() {}
                return n
            }();
            t.GetPlatformMaintainSettingNowResult = l;
            a = function() {
                function n() {}
                return n
            }();
            t.TokenModel = a;
            i = function() {
                function n() {
                    this.CheckTransferShow = 0
                }
                return n
            }();
            t.TransferDivShowModel = i;
            u = function() {
                function n() {
                    this.IsAvailable = !1;
                    this.IsDone = !1
                }
                return n
            }();
            t.MasterAccountBalanceModel = u;
            v = function() {
                function n() {}
                return n
            }();
            t.CheckGameAccountPostModel = v;
            f = function() {
                function n() {
                    this.IsAvailable = !1;
                    this.IsDone = !1
                }
                return n
            }();
            t.GameAccountBalanceModel = f;
            y = function() {
                function n() {}
                return n
            }();
            t.CheckAccountOrCreatePostModel = y;
            e = function() {
                function n() {}
                return n
            }();
            t.TranscationPostModel = e;
            p = function() {
                function n() {}
                return n
            }();
            t.ForwardGamePostModel = p;
            w = function() {
                function n() {}
                return n
            }();
            t.PlatformListModel = w;
            o = function() {
                function n() {
                    this.NewsID = "";
                    this.NewsCategory = NewsCategoryEnum.Importance;
                    this.NewsLocation = 1;
                    this.PageNumber = 0;
                    this.PageSize = 20;
                    this.OrderField = "StartTime";
                    this.Desc = "true";
                    this.WebSide = "MOBILE"
                }
                return n
            }();
            t.ImportantNewsQuery = o;
            s = function() {
                function n() {}
                return n
            }();
            t.ImportantNews = s;
            h = function() {
                function n() {
                    this.IsAvailable = !1
                }
                return n
            }();
            t.AnchorBalanceMobileModel = h
        })(t = n.Models || (n.Models = {}))
    }(OBSMobileApp || (OBSMobileApp = {})),
    function(n) {
        var t;
        (function(n) {
            var t = function() {
                function n(n) {
                    this.dataProvider = n
                }
                return n.prototype.CheckIsLogin = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/MemberInfo/CheckLogin", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data !== "" && t.Data !== null && t.Data !== undefined)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.CheckAccount = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Game/CheckAccount", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.GetMemberBalanceInfoByAccountID = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/MemberTransfer/GetMemberBalanceInfoByAccountID", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.GetGameBalanceInfoByAccountID = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Game/GetBalance", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n.Data)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.TransferPoint = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Game/TransferPoint", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.TransferMainAllAmountToGame = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Game/TransferMainAllAmountToGame", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.ForwardGame = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Game/ForwardGame", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n.Data)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.GetPlatformMaintainSettingNow = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Game/GetPlatformMaintainSettingNow", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.GetPlatformServiceInfoAvailableListByAccountID = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Game/GetPlatformServiceInfoAvailableListByAccountID", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.SignCheck = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Authorize/SignCheck", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.GetBlackList = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Common/GetBlackList", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.GetIsBet = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Common/GetIsBet", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.GetCustomerServiceComplaintListByMemberAccount = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../api/MemberInfo/GetCustomerServiceComplaintListByMemberAccount", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n.Data)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.GetNewsByCondition = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Common/GetRevealableNewsByCondition", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n.Data)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.GetAnchorBalance = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Game/GetAnchorBalanceByAccountID", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data.BalanceAmount)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.CheckPlatformTransactionMaintainSettingEnable = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Game/CheckPlatformTransactionMaintainSettingEnable?GameType=" + n, HttpMethodEnum.Post).then(function(n) {
                        t.resolve(n.Data)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.$name = "GameAreaSvc", n.$inject = ["DataProvider"], n
            }();
            n.GameAreaSvc = t
        })(t = n.Services || (n.Services = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterService(OBSMobileApp.Services.GameAreaSvc.$name, OBSMobileApp.Services.GameAreaSvc),
    function(n) {
        var t;
        (function(t) {
            var i = function() {
                function t(t, i, r, u, f, e, o, s, h, c) {
                    var w = this,
                        l, a, p, v, y;
                    this.gameAreaSvc = t;
                    this.$window = i;
                    this.messageBus = r;
                    this.blockUI = u;
                    this.qSvc = f;
                    this.timeout = e;
                    this.appContext = o;
                    this.appConfig = s;
                    this.$cookies = h;
                    this.$interval = c;
                    this.tick = null;
                    this.IsFakeMaintenance = !1;
                    this.showAVEvent = !0;
                    this.model = new n.Models.GameAreaViewModel;
                    this.gameListBlock = this.blockUI.instances.get("gameListBlock");
                    this.$interval = c;
                    this.transferBlock = this.blockUI.instances.get("transferBlock");
                    this.amountBlock = this.blockUI.instances.get("amountBlock");
                    sessionStorage.removeItem("enterkey");
                    this.messageBus.On(n.ConstDefinition.MessageBusEventName.OnGetLoggedinInfo, function(t, i) {
                        i === n.Models.LoginStatusEnum.Loggedin && w.InitializeViewModel()
                    });
                    l = this;
                    $(".transferAmount").click(function() {
                        $(".transferAmount").is(":focus") || $(".transferAmount").focus();
                        l.OnElementHeightChange(function() {
                            $("#FastTransfer").scrollTop($("#FastTransfer").prop("scrollHeight"))
                        })
                    });
                    a = !1;
                    this.$window.addEventListener("pageshow", function(n) {
                        n.persisted ? l.timeout(function() {
                            l.gameListBlock.stop()
                        }) : a && l.timeout(function() {
                            l.gameListBlock.stop()
                        })
                    });
                    this.$window.addEventListener("pagehide", function() {
                        a = !0
                    });
                    this.appContext.LoginStatus === n.Models.LoginStatusEnum.Loggedin && (p = n.Helpers.GetLocalStorageItem(n.ConstDefinition.LocalStorageKey.ImportantNewsID), this.GetNews(p));
                    v = this.$cookies.get("AVEventID");
                    v && (this.showAVEvent = $.parseJSON(v.toLowerCase()));
                    location.pathname.toLowerCase().indexOf("/game/electgame") !== -1 || location.pathname.toLowerCase().indexOf("/game/sportgame") !== -1 ? (y = n.Helpers.GetLocalStorageItem(n.ConstDefinition.LocalStorageKey.GameGroupID), y && (this.activeMenu = y, this.appContext.LoginStatus !== n.Models.LoginStatusEnum.Loggedin && n.Helpers.DeleteLocalStorageItem(n.ConstDefinition.LocalStorageKey.GameGroupID))) : n.Helpers.DeleteLocalStorageItem(n.ConstDefinition.LocalStorageKey.GameGroupID)
                }
                return t.prototype.InitializeViewModel = function() {
                    this.platformTransferEnabled = new n.Models.PlatformTransferEnabled;
                    this.appContext.UserProfile.DirectorID === "F" && (this.IsFakeMaintenance = !0)
                }, t.prototype.OnElementHeightChange = function(n) {
                    var r = $("#FastTransfer").prop("scrollHeight"),
                        i, t;
                    (function u() {
                        i = $("#FastTransfer").scrollTop();
                        r !== i && n();
                        t && clearTimeout(t);
                        $("#FastTransfer").is(":visible") && (t = setTimeout(u, 200))
                    })()
                }, t.prototype.SpecialErrorBy3D = function(t, i, r) {
                    var e, u, f, o;
                    return (i === void 0 && (i = !0), r === void 0 && (r = !1), !t) ? !1 : (e = _.find(this.model.DDDAlertErrors, function(n) {
                        return n.ErrorCodes.some(function(n) {
                            return n === t
                        })
                    }), e) ? (n.Helpers.Alert(e.Message, SweetAlertTypeEnum.warning), this.ControlTransferPanel(!1), this.ShowErrorMsgOnInputAmount(!1), !0) : (u = "", f = "", t === "-8888888888" && (this.ControlAccountPanel(GameStatusEnum.isBusy), u = n.Helpers.ChangeLanguage("繁忙中"), f = n.Helpers.ChangeLanguage("系統繁忙請稍候")), t === "-9999999999" && (u = f = n.Helpers.ChangeLanguage("載入中"), r && (this.ControlAccountPanel(GameStatusEnum.isBusy), u = n.Helpers.ChangeLanguage("繁忙中"), f = n.Helpers.ChangeLanguage("系統繁忙請稍候"))), !u && !f) ? !1 : (o = $("[block-ui=transferBlock], [block-ui=amountBlock]"), o.find("div.mask_Loading_custom").removeClass("mask_Loading_custom"), o.find("div[block-ui-container]").css("opacity", 0), this.amountBlock.start(), this.transferBlock.start(), this.ControlTransferPanel(!0), this.ShowErrorMsgOnInputAmount(!0, null, u), i && n.Helpers.Alert(f, SweetAlertTypeEnum.warning), !0)
                }, t.prototype.HideNumPad = function() {
                    var n = $(".FastTransferAmount").find(".numInputArea");
                    n.css("display", "none")
                }, t.prototype.InitializeQuickTransfer = function(n, t, i) {
                    return t === void 0 && (t = ""), i === void 0 && (i = ""), this.amountBlock.stop(), this.transferBlock.stop(), this.ControlTransferPanel(!0), this.gameType = n, this.subGameType = t, this.gameCode = i, this.ShowErrorMsgOnInputAmount(!1), this.ControlAccountPanel(GameStatusEnum.isLoading), this.GetMasterAccountBalance(), this.CheckAccountAndCheckGameAmount(), (this.gameType === "BB_LiveGame" || this.gameType === "BB_Ball" || this.subGameType === "TS_Sport" || this.subGameType === "NBBSport") && this.GetAnchorBalance(), this.tick = this.$interval(this.WaitForGetAllbalance.bind(this), 100, 0, !0, null, "interval"), !0
                }, t.prototype.fancyclose = function() {
                    this.model.TranscationPostModel.TransferAmount = null;
                    $(".keyboard").removeClass("keyIn textMoney");
                    $(".keyboard").css("color", "#999");
                    $(".mask").hide();
                    $(".mask_join").hide();
                    $(".container_main").removeAttr("style");
                    /(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent) && this.$window.scrollTo(0, 0)
                }, t.prototype.CustomerServiceComplaintListByMemberAccount = function() {
                    var t = this;
                    this.gameAreaSvc.SignCheck().then(function(i) {
                        if (i == !0) {
                            var r = t.model.GetCustomerServiceComplaintListByMemberAccountModel;
                            r.MemberAccountID = t.appContext.UserProfile.AccountID;
                            t.gameAreaSvc.GetCustomerServiceComplaintListByMemberAccount(r).then(function(i) {
                                i == !0 ? t.$window.location.href = "CustomerService/ComplianBox" : n.Helpers.Alert("5分鐘內限申請一次投訴箱，如急需諮詢，請聯繫【在線客服】", SweetAlertTypeEnum.info)
                            }).catch(function(t) {
                                n.Helpers.AlertSwitch(t)
                            })
                        } else t.$window.location.href = "CustomerService/ComplianBox"
                    }).catch(function(t) {
                        n.Helpers.AlertSwitch(t)
                    })
                }, t.prototype.RegisterValidation = function() {
                    jQuery.validator.addMethod("ckValidatorNum", function(n) {
                        return /(^[1-9]+)\d*$/.test(n)
                    })
                }, t.prototype.ConfirmMemberGameBlackList = function(n) {
                    var t, i, r;
                    return this.appContext.UserProfile.MemberPlatformBlackList == undefined ? !0 : this.appContext.UserProfile.MemberPlatformBlackList.length === 0 ? !1 : (t = $("#" + n + "_personal_maintain"), i = _.filter(this.appContext.UserProfile.MemberPlatformBlackList, function(t) {
                        return t.ServiceID === n
                    }), i.length === 0) ? !1 : i[0].BlackStatus === "0" ? (r = $(window).scrollTop(), $(".container_main").css({
                        position: "fixed",
                        top: "-" + r + "px"
                    }), t.addClass("selfcontrol"), !0) : (this.timeout(function() {
                        $(".gameMainTain_in .liveGameNG_in").hide();
                        t.show();
                        setTimeout(function() {
                            t.hide()
                        }, 3e3);
                        $(".liveGame_list").mouseout(function() {
                            t.hide()
                        })
                    }), !1)
                }, t.prototype.GetMasterAccountBalance = function() {
                    var t = this;
                    return this.model.MasterAccountBalanceModel.BalanceAmount = n.Helpers.ChangeLanguage("載入中"), this.ControlTransferPanel(!0), this.model.MasterAccountBalanceModel.IsDone = !1, this.gameAreaSvc.GetMemberBalanceInfoByAccountID().then(function(i) {
                        t.model.MasterAccountBalanceModel.IsAvailable = !0;
                        t.model.MasterAccountBalanceModel.BalanceAmount = n.Formatter.NumberFormat(Math.floor(Number(i.Data.BalanceAmount)));
                        t.tempMainBalance = Math.floor(Number(i.Data.BalanceAmount));
                        t.ControlTransferPanel()
                    }).catch(function(n) {
                        n.Error.Code === 4001 && (t.model.MasterAccountBalanceModel.IsAvailable = !1, t.ControlTransferPanel(!0), t.ControlAccountPanel(GameStatusEnum.isMaintain));
                        t.ShowErrorMsgOnInputAmount(!0, n.Error.Message)
                    }).finally(function() {
                        t.model.MasterAccountBalanceModel.IsDone = !0
                    }), !0
                }, t.prototype.ChangePlatformName = function(n) {
                    return $("#Pltform" + n).val()
                }, t.prototype.GetGameAccountBalance = function() {
                    var t = this,
                        i;
                    return this.model.GameAccountBalanceModel.GameBalanceAmount = n.Helpers.ChangeLanguage("載入中"), this.model.GameAccountBalanceModel.IsAvailable = !1, i = new n.Models.CheckGameAccountPostModel, i.GameType = this.gameType, this.model.GameAccountBalanceModel.IsDone = !1, this.gameAreaSvc.GetGameBalanceInfoByAccountID(i).then(function(i) {
                        t.model.GameAccountBalanceModel.IsAvailable = !0;
                        t.model.GameAccountBalanceModel.GameBalanceAmount = n.Formatter.NumberFormat(Math.floor(Number(i)));
                        t.ControlTransferPanel()
                    }).catch(function(n) {
                        (n.Error.Code === 4001 && (t.model.GameAccountBalanceModel.IsAvailable = !1, t.ControlTransferPanel(!0), t.ControlAccountPanel(GameStatusEnum.isMaintain)), t.gameType === "DDD" && t.SpecialErrorBy3D(n.Error.Message, !1)) || t.ShowErrorMsgOnInputAmount(!0, n.Error.Message)
                    }).finally(function() {
                        t.model.GameAccountBalanceModel.IsDone = !0
                    }), !0
                }, t.prototype.CheckAccountAndCheckGameAmount = function() {
                    var t = this,
                        i = new n.Models.CheckAccountOrCreatePostModel;
                    return this.model.GameAccountBalanceModel.GameBalanceAmount = n.Helpers.ChangeLanguage("載入中"), this.model.GameAccountBalanceModel.IsAvailable = !1, i.GameType = this.gameType, i.SubGameType = this.subGameType, this.ControlTransferPanel(!0), this.model.GameAccountBalanceModel.IsDone = !1, this.gameAreaSvc.CheckAccount(i).then(function() {
                        t.gameAreaSvc.GetGameBalanceInfoByAccountID(i).then(function(i) {
                            t.model.GameAccountBalanceModel.IsAvailable = !0;
                            t.model.GameAccountBalanceModel.GameBalanceAmount = n.Formatter.NumberFormat(Math.floor(Number(i)));
                            t.ControlTransferPanel();
                            var r = function() {
                                t.ShowErrorMsgOnInputAmount(!0, n.Helpers.ChangeLanguage("很抱歉，目前轉帳功能維護中"), n.Helpers.ChangeLanguage("很抱歉，目前轉帳功能維護中"));
                                t.ControlTransferPanel(!0, !0)
                            };
                            if (t.appConfig.CompetenceModel.IsEnable === !1) {
                                r();
                                return
                            }
                            t.platformTransferEnabled.Reset();
                            t.gameAreaSvc.CheckPlatformTransactionMaintainSettingEnable(t.gameType).then(function() {
                                t.platformTransferEnabled.IsAvailable = !0
                            }).catch(function() {
                                r()
                            }).finally(function() {
                                t.platformTransferEnabled.IsApiDone = !0
                            })
                        }).catch(function(n) {
                            (t.model.GameAccountBalanceModel.IsAvailable = !1, t.ControlTransferPanel(), t.gameType === "DDD" && t.SpecialErrorBy3D(n.Error.Message, !1)) || t.ShowErrorMsgOnInputAmount(!0, "", n.Error.Message)
                        }).finally(function() {
                            t.model.GameAccountBalanceModel.IsDone = !0
                        })
                    }).catch(function(i) {
                        i.Error.Code === 4008 ? (t.ShowErrorMsgOnInputAmount(!0, i.Error.Message, i.Error.Message), t.ControlTransferPanel(!0)) : i.Error.Code === 4001 ? (t.ControlAccountPanel(GameStatusEnum.isMaintain), t.model.GameAccountBalanceModel.IsAvailable = !0, t.ControlTransferPanel(!0), t.ShowErrorMsgOnInputAmount(!0, i.Error.Message)) : i.Error.Code === 1002 ? n.Helpers.ChangeLanguage(i.Error.Message) === n.Helpers.ChangeLanguage("您的可用額度不足") ? (n.Helpers.AlertSwitch(i), t.ControlTransferPanel(!1)) : (t.ControlAccountPanel(GameStatusEnum.isMaintain), t.ShowErrorMsgOnInputAmount(!0, i.Error.Message), t.ControlTransferPanel(!0)) : i.Error.Code === 5999 ? (t.ControlTransferPanel(!0), t.ControlAccountPanel(GameStatusEnum.isMaintain), t.ShowErrorMsgOnInputAmount(!0, i.Error.Message)) : i.Error.Code === 4015 ? (t.ShowErrorMsgOnInputAmount(!1, i.Error.Message, i.Error.Message, !0), t.ControlTransferPanel(!0)) : (n.Helpers.AlertSwitch(i), t.ControlTransferPanel(!1))
                    }), !0
                }, t.prototype.TransferFromAccountToGame = function() {
                    var t = this,
                        i = new n.Models.TranscationPostModel;
                    return (i.TransferAmount = this.model.TranscationPostModel.TransferAmount, this.model.TranscationPostModel.TransferAmount > n.Formatter.GetNumberData(this.model.MasterAccountBalanceModel.BalanceAmount)) ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("您的可用額度不足"), SweetAlertTypeEnum.warning, !1, "", null, function() {
                        t.ShowKeyboardTransferAndCleanAccount()
                    }), !1) : this.model.TranscationPostModel.TransferAmount === 0 || !this.model.TranscationPostModel.TransferAmount ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("請輸入轉點金額"), SweetAlertTypeEnum.warning), !1) : (i.GameType = this.gameType, i.SubGameType = this.subGameType, this.ControlTransferPanel(!0), this.gameAreaSvc.TransferPoint(i).then(function() {
                        t.model.GameAccountBalanceModel.GameBalanceAmount = n.Formatter.NumberFormat(n.Formatter.GetNumberData(t.model.GameAccountBalanceModel.GameBalanceAmount) + t.model.TranscationPostModel.TransferAmount);
                        t.fancyclose();
                        n.Helpers.Alert(n.Helpers.ChangeLanguage("轉點成功"), SweetAlertTypeEnum.success, !1, "", null, function(n) {
                            return t.ComfirmEnterGame(n)
                        }, n.Helpers.ChangeLanguage("進入遊戲"), n.Helpers.ChangeLanguage("關閉視窗"));
                        t.model.TranscationPostModel.TransferAmount = null
                    }).catch(function(r) {
                        if (r === null || r === undefined) {
                            n.Helpers.Alert("請求異常，請稍後再試", SweetAlertTypeEnum.warning);
                            t.model.TranscationPostModel.TransferAmount = null;
                            t.ControlTransferPanel(!1);
                            return
                        }
                        t.gameType === "DDD" && t.SpecialErrorBy3D(r.Error.Message, !0, !0) || (r.Error.Code === 4008 ? n.Helpers.Alert(r.Error.Message, SweetAlertTypeEnum.warning, !1, "", null, function() {
                            t.fancyclose();
                            window.location.reload(!0)
                        }) : r.Error.Code === ApiStatusCodeEnum.PlatformMutualTransferReachedNumberLimit ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("今日互轉次數已達到限定值"), SweetAlertTypeEnum.info), t.model.TranscationPostModel.TransferAmount = null, t.ControlTransferPanel(!1), t.ShowErrorMsgOnInputAmount(!1)) : r.Error.Code === ApiStatusCodeEnum.PlatformMutualTransferReachedMoneyLimit ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("今日互轉額度已達限定值"), SweetAlertTypeEnum.info), t.model.TranscationPostModel.TransferAmount = null, t.ControlTransferPanel(!1)) : r.Error.Code === 1002 ? n.Helpers.ChangeLanguage(r.Error.Message) === n.Helpers.ChangeLanguage("您存入的額度高於上限") ? (n.Helpers.Alert("您存入的額度高於上限", SweetAlertTypeEnum.info), t.model.TranscationPostModel.TransferAmount = null, t.ControlTransferPanel(!1)) : n.Helpers.ChangeLanguage(r.Error.Message) === n.Helpers.ChangeLanguage("您的可用額度不足") ? (n.Helpers.Alert("您的可用額度不足", SweetAlertTypeEnum.info, !1, "", null, function() {
                            t.ShowKeyboardTransferAndCleanAccount()
                        }), t.ControlTransferPanel(!1)) : n.Helpers.ChangeLanguage(r.Error.Message) === n.Helpers.ChangeLanguage("代理商金額不足") ? (n.Helpers.Alert(n.Helpers.StringFormat(n.Helpers.ChangeLanguage("【{0}】與【{1}】互轉失敗，請聯繫客服！"), n.Helpers.ChangeLanguage("主帳戶"), t.ChangePlatformName(i.GameType)), SweetAlertTypeEnum.info, !1, "", null, function() {
                            t.ShowKeyboardTransferAndCleanAccount()
                        }), t.ControlTransferPanel(!1)) : (t.ControlAccountPanel(GameStatusEnum.isMaintain), t.ShowErrorMsgOnInputAmount(!0, r.Error.Message), t.ControlTransferPanel(!0)) : r.Error.Code === 4015 ? n.Helpers.Alert(r.Error.Message, SweetAlertTypeEnum.warning, !1, "", null, function() {
                            t.fancyclose();
                            window.location.reload(!0)
                        }) : r.Error.Code === 1004 ? (n.Helpers.AlertSwitch(r), t.ShowErrorMsgOnInputAmount(!0, r.Error.Message, r.Error.Message), t.ControlTransferPanel(!0, !0)) : r.Error.Code === 4001 ? n.Helpers.Alert(r.Error.Message, SweetAlertTypeEnum.warning, !1, "", null, function() {
                            t.fancyclose();
                            window.location.reload(!0)
                        }) : (t.ControlTransferPanel(!1), n.Helpers.AlertSwitch(r)))
                    }), !0)
                }, t.prototype.TransferMainAllAmountToGame = function() {
                    var t = this,
                        i = new n.Models.TransferMainAllAmountToGamePostModel;
                    return n.Formatter.GetNumberData(this.model.MasterAccountBalanceModel.BalanceAmount) === 0 ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("主帳戶餘額不足"), SweetAlertTypeEnum.warning), !1) : (i.GameType = this.gameType, i.SubGameType = this.subGameType, this.ControlTransferPanel(!0), this.ShowErrorMsgOnInputAmount(!0, "", n.Helpers.ChangeLanguage("轉點中") + "...", !0), this.gameAreaSvc.TransferMainAllAmountToGame(i).then(function() {
                        t.model.GameAccountBalanceModel.GameBalanceAmount = n.Formatter.NumberFormat(n.Formatter.GetNumberData(t.model.GameAccountBalanceModel.GameBalanceAmount) + n.Formatter.GetNumberData(t.model.MasterAccountBalanceModel.BalanceAmount));
                        t.fancyclose();
                        n.Helpers.Alert(n.Helpers.ChangeLanguage("轉點成功"), SweetAlertTypeEnum.success, !1, "", null, function(n) {
                            return t.ComfirmEnterGame(n)
                        }, n.Helpers.ChangeLanguage("進入遊戲"), n.Helpers.ChangeLanguage("關閉視窗"));
                        t.model.TranscationPostModel.TransferAmount = null
                    }).catch(function(r) {
                        if (r === null || r === undefined) {
                            n.Helpers.Alert("請求異常，請稍後再試", SweetAlertTypeEnum.warning);
                            t.ControlTransferPanel(!1);
                            t.ShowErrorMsgOnInputAmount(!1);
                            return
                        }
                        if (t.gameType !== "DDD" || !t.SpecialErrorBy3D(r.Error.Message, !0, !0)) {
                            if (r.Error.Code === 4008) {
                                n.Helpers.Alert(r.Error.Message, SweetAlertTypeEnum.warning, !1, "", null, function() {
                                    t.fancyclose();
                                    window.location.reload(!0)
                                });
                                return
                            }
                            r.Error.Code === ApiStatusCodeEnum.PlatformMutualTransferReachedNumberLimit ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("今日互轉次數已達到限定值"), SweetAlertTypeEnum.info), t.model.TranscationPostModel.TransferAmount = null, t.ControlTransferPanel(!1), t.ShowErrorMsgOnInputAmount(!1)) : r.Error.Code === ApiStatusCodeEnum.PlatformMutualTransferReachedMoneyLimit ? (n.Helpers.Alert(n.Helpers.ChangeLanguage("今日互轉額度已達限定值"), SweetAlertTypeEnum.info), t.model.TranscationPostModel.TransferAmount = null, t.ControlTransferPanel(!1), t.ShowErrorMsgOnInputAmount(!1)) : r.Error.Code === 1002 ? n.Helpers.ChangeLanguage(r.Error.Message) === n.Helpers.ChangeLanguage("您存入的額度高於上限") ? (n.Helpers.Alert("您存入的額度高於上限", SweetAlertTypeEnum.info), t.model.TranscationPostModel.TransferAmount = null, t.ControlTransferPanel(!1), t.ShowErrorMsgOnInputAmount(!1)) : n.Helpers.ChangeLanguage(r.Error.Message) === n.Helpers.ChangeLanguage("您的可用額度不足") ? (n.Helpers.Alert("您的可用額度不足", SweetAlertTypeEnum.info, !1, "", null, function() {
                                t.ShowKeyboardTransferAndCleanAccount()
                            }), t.ControlTransferPanel(!1), t.ShowErrorMsgOnInputAmount(!1)) : n.Helpers.ChangeLanguage(r.Error.Message) === n.Helpers.ChangeLanguage("代理商金額不足") ? (n.Helpers.Alert(n.Helpers.StringFormat(n.Helpers.ChangeLanguage("【{0}】與【{1}】互轉失敗，請聯繫客服！"), n.Helpers.ChangeLanguage("主帳戶"), t.ChangePlatformName(i.GameType)), SweetAlertTypeEnum.info, !1, "", null, function() {
                                t.ShowKeyboardTransferAndCleanAccount()
                            }), t.ControlTransferPanel(!1), t.ShowErrorMsgOnInputAmount(!1)) : (t.ControlAccountPanel(GameStatusEnum.isMaintain), t.ShowErrorMsgOnInputAmount(!0, r.Error.Message), t.ControlTransferPanel(!0)) : r.Error.Code === 4015 ? (t.ControlTransferPanel(!1), t.ShowErrorMsgOnInputAmount(!1), n.Helpers.AlertSwitch(r)) : r.Error.Code === 1004 ? (n.Helpers.AlertSwitch(r), t.ShowErrorMsgOnInputAmount(!0, r.Error.Message, r.Error.Message), t.ControlTransferPanel(!0, !0)) : r.Error.Code === 4001 ? n.Helpers.Alert(r.Error.Message, SweetAlertTypeEnum.warning, !1, "", null, function() {
                                t.fancyclose();
                                window.location.reload(!0)
                            }) : (t.ControlTransferPanel(!1), t.ShowErrorMsgOnInputAmount(!1), n.Helpers.AlertSwitch(r))
                        }
                    }), !0)
                }, t.prototype.EnterGame = function() {
                    this.gameListBlock.start();
                    switch (this.gameType) {
                        case "AGI":
                            switch (this.subGameType) {
                                case "AGIFishing":
                                    if (n.Formatter.GetNumberData(this.model.GameAccountBalanceModel.GameBalanceAmount) === 0) {
                                        n.Helpers.Alert(n.Helpers.ChangeLanguage("您沒有額度，請先進行轉帳！"), SweetAlertTypeEnum.warning);
                                        this.gameListBlock.stop();
                                        return
                                    }
                            }
                            this.OpenGame();
                            break;
                        case "TS":
                        case "Ameba":
                        case "BNG":
                        case "DT":
                        case "BB_Ball":
                        case "DDD":
                        case "BB_LiveGame":
                        case "VR":
                        case "KY":
                            switch (this.subGameType) {
                                case "KYFishing":
                                    this.OpenGame();
                                    break;
                                default:
                                    this.RedirectToGame()
                            }
                            break;
                        case "LC":
                            switch (this.subGameType) {
                                case "LCFishing":
                                    this.OpenGame();
                                    break;
                                default:
                                    this.RedirectToGame()
                            }
                            break;
                        case "PLS":
                        case "NBB":
                        case "PS":
                        case "SM":
                            this.RedirectToGame();
                            break;
                        case "AGIN":
                        case "BBIN":
                        case "NewBBIN":
                        case "AllBet":
                        case "CMD":
                        case "OOG":
                        case "NewOG":
                        case "WM":
                        case "GPI":
                        case "DG":
                        case "SA":
                        case "OneBook":
                        case "AVIA":
                        case "IMone":
                        case "AES":
                            this.OpenGame();
                            break;
                        case "CQNine":
                            this.subGameType === "CQNineFishing" ? this.OpenGame() : this.RedirectToGame();
                            break;
                        case "DS":
                            switch (this.subGameType) {
                                case "DSFishing":
                                    this.OpenGame();
                                    break;
                                default:
                                    this.RedirectToGame()
                            }
                    }
                }, t.prototype.ComfirmEnterGame = function(n) {
                    n && this.EnterGame()
                }, t.prototype.OpenGame = function() {
                    var n = new Date,
                        t = "//" + location.host + "/CheckGame?gameType=" + this.gameType + "&subGameType=" + this.subGameType + "&gameCode=" + this.gameCode + "&isMobile=true&dt=" + this.appContext.UserProfile.AccountID + n.getTime();
                    this.fancyclose();
                    this.gameListBlock.stop();
                    this.$window.open(t, this.subGameType || "game")
                }, t.prototype.RedirectToGame = function() {
                    var n = new Date,
                        t = "//" + location.host + "/CheckGame?gameType=" + this.gameType + "&subGameType=" + this.subGameType + "&gameCode=" + this.gameCode + "&isMobile=true&dt=" + this.appContext.UserProfile.AccountID + n.getTime();
                    this.fancyclose();
                    location.href = t
                }, t.prototype.ControlTransferPanel = function(n, t) {
                    var i = this;
                    this.timeout(function() {
                        n || i.model.MasterAccountBalanceModel.IsAvailable && i.model.MasterAccountBalanceModel.BalanceAmount === "0" ? $(".numInputArea").addClass("lock") : $(".numInputArea").removeClass("lock");
                        var r = $(".transferAmount, .btn_popupW50L, .btn_popupW100");
                        n ? (i.model.TransferDivShowModel.CheckTransferShow = 0, i.model.TransferDivAllTransferShowModel.CheckTransferShow = 0, r.attr("disabled", "disabled").css("cssText", "background-color:#bbb !important;color:white !important;"), $("#FastTransferAmount").attr("disabled", "disabled").css("cssText", "background-color:#e5e5e5 !important;color:white !important;").removeClass("on"), t && $(".btn_popupW50L, .btn_popupW100").removeAttr("disabled style"), $(".btn_transfer").attr("disabled", "disabled").css("cssText", "background-color:#bbb !important;color:white !important;")) : i.model.MasterAccountBalanceModel.IsAvailable && i.model.GameAccountBalanceModel.IsAvailable ? (r.removeAttr("disabled style"), $("#FastTransferAmount").removeAttr("disabled style"), i.model.MasterAccountBalanceModel.BalanceAmount != "0" ? ($(".btn_transfer").removeAttr("disabled style"), $("#FastTransferAmount").focus(), i.model.TransferDivAllTransferShowModel.CheckTransferShow = 1, i.model.TransferDivShowModel.CheckTransferShow = 1) : i.model.MasterAccountBalanceModel.BalanceAmount == "0" && $("#FastTransferAmount").attr("disabled", "disabled").css("cssText", "background-color:#e5e5e5 !important;color:white !important;")) : (i.model.TransferDivShowModel.CheckTransferShow = 0, i.model.TransferDivAllTransferShowModel.CheckTransferShow = 0, r.attr("disabled", "disabled").css("cssText", "background-color:#bbb !important;color:white !important;"), $(".btn_transfer").attr("disabled", "disabled").css("cssText", "background-color:#bbb !important;color:white !important;"), $("#FastTransferAmount").css("cssText", "background-color:#e5e5e5 !important;color:white !important;"))
                    })
                }, t.prototype.ControlAccountPanel = function(t, i) {
                    switch (t) {
                        case GameStatusEnum.isLoading:
                            this.model.MasterAccountBalanceModel.BalanceAmount = i ? i : n.Helpers.ChangeLanguage("載入中");
                            this.model.GameAccountBalanceModel.GameBalanceAmount = i ? i : n.Helpers.ChangeLanguage("載入中");
                            break;
                        case GameStatusEnum.isMaintain:
                            this.model.GameAccountBalanceModel.GameBalanceAmount = i ? i : n.Helpers.ChangeLanguage("維護中");
                            break;
                        case GameStatusEnum.isBusy:
                            this.model.GameAccountBalanceModel.GameBalanceAmount = i ? i : n.Helpers.ChangeLanguage("繁忙中")
                    }
                }, t.prototype.ChangePlatformNameOnPC = function(n) {
                    return $("#Pltform" + n).val()
                }, t.prototype.ShowErrorMsgOnInputAmount = function(t, i, r, u) {
                    r === void 0 && (r = null);
                    u === void 0 && (u = !1);
                    i = r == null ? n.Helpers.ChangeLanguage("維護中") : r;
                    n.Helpers.StringContainsOneOfKeywords(i, "平台轉帳維護中", "轉帳", "轉帳功能維護中", "很抱歉，目前轉帳功能維護中") && (i = n.Helpers.ChangeLanguage("轉帳維護中"));
                    this.model.TranscationPostModel.TransferAmount = null;
                    t ? (this.messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnMobileKeyboardOpen, {
                        elemId: "FastTransferAmount",
                        text: i
                    }), $("#FastTransferAmount").addClass("placeholder_class")) : (this.messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnMobileKeyboardOpen, {
                        elemId: "FastTransferAmount",
                        text: n.Helpers.ChangeLanguage("請輸入金額")
                    }), $("#FastTransferAmount").css("color", "#999"))
                }, t.prototype.GetFloor = function(n) {
                    return Math.floor(n)
                }, t.prototype.ShowKeyboardTransferAndCleanAccount = function() {
                    var t = this;
                    this.timeout(function() {
                        t.model.TranscationPostModel.TransferAmount = null;
                        t.messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnMobileKeyboardOpen, {
                            elemId: "FastTransferAmount",
                            text: n.Helpers.ChangeLanguage("請輸入金額")
                        })
                    })
                }, t.prototype.GetNews = function(n) {
                    var t = this;
                    this.gameAreaSvc.GetNewsByCondition(this.model.NewsQueryModel).then(function(i) {
                        i != undefined && i != null && i.length > 0 && (t.model.ImportantNewsItem = i[0], n != t.model.ImportantNewsItem.NewsID && (jQuery("#importantNews").show(), jQuery("#importantNewsContent").html(t.model.ImportantNewsItem.NewsContent)))
                    }).catch(function() {})
                }, t.prototype.CloseImportantNews = function() {
                    n.Helpers.SetLocalStorageItem(n.ConstDefinition.LocalStorageKey.ImportantNewsID, this.model.ImportantNewsItem.NewsID, !0);
                    $("#importantNews").hide()
                }, t.prototype.OnShowGames = function(t) {
                    n.Helpers.SetLocalStorageItem(n.ConstDefinition.LocalStorageKey.GameGroupID, t, !0);
                    this.activeMenu = t
                }, t.prototype.GetMenuCss = function(n, t) {
                    var i, r = t % 2 == 0;
                    return this.activeMenu ? i = this.IsActiveMenu(n) : (i = r, i && (this.activeMenu = n)), {
                        on: i,
                        w50L: r,
                        "w50R t_orange": !r
                    }
                }, t.prototype.IsActiveMenu = function(n) {
                    return this.activeMenu ? this.activeMenu === n : !0
                }, t.prototype.GetAnchorBalance = function() {
                    var t = this;
                    return this.gameAreaSvc.GetAnchorBalance().then(function(i) {
                        t.model.AnchorBalanceModel.IsAvailable = !0;
                        t.model.AnchorBalanceModel.BalanceAmount = n.Formatter.NumberFormat(Math.floor(Number(i)))
                    }).catch(function() {}), !0
                }, t.prototype.WaitForGetAllbalance = function() {
                    if (this.model.GameAccountBalanceModel.IsDone !== !1 && this.model.MasterAccountBalanceModel.IsDone !== !1 && this.platformTransferEnabled.IsApiDone !== !1 && (this.$interval.cancel(this.tick), this.model.GameAccountBalanceModel.IsAvailable && this.model.MasterAccountBalanceModel.IsAvailable && this.platformTransferEnabled.IsAvailable && this.tempMainBalance > 0)) {
                        var n = $(".FastTransferAmount").find(".numInputArea");
                        n.css("display", "table")
                    }
                }, t.prototype.CloseAVEvent = function(n) {
                    n.target.className.contains("btn_closeAV") && (this.$cookies.put("AVEventID", "false", {
                        path: "/"
                    }), this.showAVEvent = !1)
                }, t.prototype.AVEventUrl = function(n, t) {
                    n.target.className.contains("btn_AV") && window.open(t)
                }, t.prototype.ShowLogIn = function() {
                    jQuery("#popup_login").show()
                }, t.$name = "GameAreaCtrl", t.$inject = ["GameAreaSvc", "$window", "messageBus", "blockUI", "$q", "$timeout", "appContext", "appConfig", "$cookies", "$interval"], t
            }();
            t.GameAreaCtrl = i
        })(t = n.Controllers || (n.Controllers = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterController(OBSMobileApp.Controllers.GameAreaCtrl.$name, OBSMobileApp.Controllers.GameAreaCtrl),
    function(n) {
        var t;
        (function(n) {
            var t = function() {
                function n() {}
                return n
            }();
            n.HeaderViewModel = t
        })(t = n.Models || (n.Models = {}))
    }(OBSMobileApp || (OBSMobileApp = {})),
    function(n) {
        var t;
        (function(n) {
            var t = function() {
                function n(n, t) {
                    this.dataProvider = n;
                    this.xpagerSvc = t
                }
                return n.$name = "HeaderSvc", n.$inject = ["DataProvider", "XPagerSvc"], n
            }();
            n.HeaderService = t
        })(t = n.Services || (n.Services = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterService(OBSMobileApp.Services.HeaderService.$name, OBSMobileApp.Services.HeaderService),
    function(n) {
        var t;
        (function(t) {
            var i = function() {
                function t(t, i, r, u, f, e, o, s, h) {
                    var c = this;
                    this.appConfig = t;
                    this.appContext = i;
                    this.$q = r;
                    this.$timeout = u;
                    this.messageBus = f;
                    this.adapter = e;
                    this.blockUI = o;
                    this.dataProvider = s;
                    this.headerSvc = h;
                    this.CheckCanFunction = "";
                    this.isFakeTransferMaintenance = !1;
                    this.headerMaintainShow = !1;
                    this.headerMaintainTimer = null;
                    this.isShowVIPAppBtn = !1;
                    this.IsFAccount = !1;
                    n.Helpers.GetMobileSystem().isIOS && !this.appContext.UserProfile.IsNewMember && this.appContext.UserProfile.MemberStatus === 1 && (this.isShowVIPAppBtn = !0);
                    window.onpageshow = function(n) {
                        n.persisted && window.location.reload()
                    };
                    this.appContext.UserProfile && this.appContext.UserProfile.DirectorID === "F" && (this.IsFAccount = !0);
                    this.messageBus.On(n.ConstDefinition.MessageBusEventName.OnHeaderGoBack, function() {
                        c.HeaderGoBack()
                    })
                }
                return t.prototype.RegisterAdditionallyGoBack = function() {
                    this.messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnRegisterAdditionallyGoBack, null)
                }, t.prototype.MemberWithdrawalGoBack = function() {
                    this.messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnMemberWithdrawalGoBack, null)
                }, t.prototype.MemberDepositGoBack = function() {
                    this.messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnMemberDepositGoBack, null)
                }, t.prototype.RegisterValidation = function() {}, t.prototype.PopupClose = function(n) {
                    jQuery("#" + n).hide()
                }, t.prototype.ClickCheckFunction = function(n) {
                    var t = this;
                    this.$timeout.cancel(this.headerMaintainTimer);
                    this.CheckCanFunction = n;
                    this.headerMaintainShow = !0;
                    this.headerMaintainTimer = this.$timeout(function() {
                        t.headerMaintainShow = !1
                    }, 3e3)
                }, t.prototype.HeaderGoBack = function() {
                    n.NavigationHelper.GetInstance().GoPreviousPage()
                }, t.prototype.CheckBlackListKUIM = function() {
                    if (this.appContext.UserProfile.MemberPlatformBlackList == undefined || this.appContext.UserProfile.MemberPlatformBlackList.length == 0) return !1;
                    var t = this.appContext.UserProfile.MemberPlatformBlackList,
                        n = _.filter(t, function(n) {
                            return n.ServiceID == "KUIM"
                        });
                    return n.length == 0 ? !1 : n[0].BlackStatus == "0"
                }, t.prototype.IsMemberStatusAudit = function() {
                    return this.appContext.UserProfile.MemberStatus == n.Models.MemberStatusEnum.Audit
                }, t.prototype.IsMemberStatusWaitForDeposit = function() {
                    return this.appContext.UserProfile.MemberStatus == n.Models.MemberStatusEnum.WaitForDeposit
                }, t.prototype.IsShowKUIM = function() {
                    return this.IsMemberStatusAudit() || this.IsMemberStatusWaitForDeposit() ? !1 : this.CheckBlackListKUIM()
                }, t.prototype.IsShowKUIMMaintain = function() {
                    return this.IsMemberStatusAudit() || this.IsMemberStatusWaitForDeposit() ? !0 : this.CheckBlackListKUIM()
                }, t.$name = "HeaderCtrl", t.$inject = ["appConfig", "appContext", "$q", "$timeout", "messageBus", "SignalRAdapter", "blockUI", "DataProvider", "HeaderSvc"], t
            }();
            t.HeaderCtrl = i
        })(t = n.Controllers || (n.Controllers = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterController(OBSMobileApp.Controllers.HeaderCtrl.$name, OBSMobileApp.Controllers.HeaderCtrl),
    function(n) {
        var t;
        (function(n) {
            var r = function() {
                    function t() {
                        this.IsShowBalanceAmount = !1;
                        this.LoginStatus = n.LoginStatusEnum.Checking;
                        this.BalanceAmount = 0;
                        this.UserLevelNumber = 0;
                        this.UserLevel = "";
                        this.MessageBoxCount = 0;
                        this.IsCanNextGetBalance = !0
                    }
                    return t
                }(),
                t, i;
            n.LoginAreaViewModel = r;
            t = function() {
                function n() {}
                return n
            }();
            n.Language = t;
            i = function() {
                function n() {}
                return n
            }();
            n.GetBalancePost = i
        })(t = n.Models || (n.Models = {}))
    }(OBSMobileApp || (OBSMobileApp = {})),
    function(n) {
        var t;
        (function(n) {
            var t = function() {
                function n() {}
                return n.DirectiveFactory = function(n) {
                    return {
                        restrict: "A",
                        link: function(t, i, r) {
                            var s, e, h = !1,
                                c = function(i) {
                                    r.balanceSlideFunc && (s = n(r.balanceSlideFunc), s(t, {
                                        $event: i
                                    }))
                                };
                            r.slideOpenCheck && (e = n(r.slideOpenCheck));
                            var o = $(window).scrollTop(),
                                u = $(".inforMDrop"),
                                l = $(".inforMDropOUT"),
                                a = $(".inforMDrop,.inforMDropOUT"),
                                f = $(".icon_inforMoney"),
                                v = function(n) {
                                    if (!e || (h = e(t, {
                                            $event: n
                                        }), h)) {
                                        if (u.css("display") === "none") {
                                            u.slideDown(200);
                                            f.addClass("on");
                                            l.show();
                                            $(".container_main:first").css({
                                                position: "fixed",
                                                top: "-" + o + "px"
                                            });
                                            l.on("click touchstart", function(n) {
                                                n.target.id !== "transferGamesPoint" && (f.removeClass("on"), a.hide(), $(".container_main:first").removeAttr("style"), $(window).scrollTop(o), $("body").css("-webkit-overflow-scrolling", "touch"), n.stopPropagation(), n.preventDefault())
                                            }).on("click touchstart", ".inforMDrop", function(n) {
                                                n.stopPropagation()
                                            })
                                        } else u.slideUp(200), f.removeClass("on"), a.hide(), $(".container_main:first").removeAttr("style"), $(window).scrollTop(o), $("body").css("-webkit-overflow-scrolling", "touch");
                                        c(n)
                                    }
                                },
                                y = function(n) {
                                    if (u.length !== 0 && u.css("display") !== "none" && !(jQuery("#clickTransferGamesPointToMain").length > 0)) {
                                        var t = $(n.target);
                                        t.parents("#GameMenu").length === 1 || t.attr("ID") === "GameMenu" || t.hasClass("swal2-container") || t.hasClass("swal2-confirm") || t.hasClass("mask_Loading_custom") || t.hasClass("swal2-title") || t.hasClass("swal2-popup") || t.hasClass("swal2-modal") || t.hasClass("swal2-noanimation") || t.hasClass("swal2-center") || t.hasClass("swal2-shown") || t.hasClass("fancybox-confirm-button") || (u.slideUp(200), f.removeClass("on"), c(n))
                                    }
                                };
                            i.click(v);
                            jQuery(document).click(y)
                        }
                    }
                }, n.$name = "balanceSlide", n.$inject = ["$parse", n.DirectiveFactory], n
            }();
            n.BalanceSlide = t
        })(t = n.Directives || (n.Directives = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterDirective(OBSMobileApp.Directives.BalanceSlide.$name, OBSMobileApp.Directives.BalanceSlide.$inject),
    function(n) {
        var t;
        (function(n) {
            var t = function() {
                function n(n, t) {
                    this.dataProvider = n;
                    this.xpagerSvc = t
                }
                return n.prototype.SignOut = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Authorize/SignOut", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.GetMemberBalanceInfoByAccountID = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/MemberTransfer/GetMemberBalanceInfoByAccountID", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.GetGameBalanceInfoByAccountID = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Game/GetBalance", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n.Data)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.TransferGamesPointToMain = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Game/TransferGamesPointToMain", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.GetAllBalance = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Game/GetBalance", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n.Data)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.GetPlatformServiceInfoAvailableListByAccountID = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Game/GetPlatformServiceInfoAvailableListByAccountID", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.$name = "LoginAreaSvc", n.$inject = ["DataProvider", "XPagerSvc"], n
            }();
            n.LoginAreaSvc = t
        })(t = n.Services || (n.Services = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterService(OBSMobileApp.Services.LoginAreaSvc.$name, OBSMobileApp.Services.LoginAreaSvc),
    function(n) {
        var t;
        (function(t) {
            var i = function() {
                function t(t, i, r, u, f, e, o, s, h) {
                    var c = this;
                    this.$timeout = t;
                    this.$interval = i;
                    this.appConfig = r;
                    this.appContext = u;
                    this.appContextSvc = f;
                    this.messageBus = e;
                    this.adapter = o;
                    this.blockUI = s;
                    this.loginAreaSvc = h;
                    this.isTransferGamesPointToMain = !0;
                    this.InitializeViewModel();
                    this.messageBus.On(n.ConstDefinition.MessageBusEventName.OnCheckNeedKickLoginStatus, function(t, i) {
                        i !== 1001 && (i !== 4009 && i !== 4010 && c.loginAreaSvc.SignOut(), c.appContextSvc.StopCheckInterval(), c.appContextSvc.ResetUserProfile(), c.model.LoginStatus = n.Models.LoginStatusEnum.NotLogin, c.$interval.cancel(c.checkBalanceInterval), c.adapter != null && c.adapter.IsConnect() && c.adapter.Disconnect(), c.appContextSvc.CheckLoginStatus(i))
                    });
                    this.messageBus.On(n.ConstDefinition.MessageBusEventName.OnGetLoggedinInfo, function(t, i) {
                        i === n.Models.LoginStatusEnum.Loggedin ? (c.model.LoginStatus = n.Models.LoginStatusEnum.Loggedin, c.model.LoginInfo = c.appContext.UserProfile, c.model.LoginInfo.MainAccountBalance = Math.floor(c.model.LoginInfo.MainAccountBalance), c.appContextSvc.StartCheckInterval(), c.GetMainBalance(), c.InitializeAdapter(), c.InitializeCountDown(), c.GetMemberPlatformAvailableGameList(), c.model.UserLevelNumber = c.appContext.UserProfile.LevelType, c.model.UserLevel = c.appContext.UserProfile.LevelTypeName) : i === n.Models.LoginStatusEnum.NotLogin ? n.Helpers.AlertSwitch({
                            Error: {
                                Code: 4e3,
                                Message: "未授權"
                            }
                        }) : i === n.Models.LoginStatusEnum.Dney ? n.Helpers.AlertSwitch({
                            Error: {
                                Code: 4005,
                                Message: "您帳號已被禁止登入，已將您登出"
                            }
                        }) : i === n.Models.LoginStatusEnum.Error && (window.location.href = "/Mobile/Home/Index")
                    });
                    this.messageBus.On(n.ConstDefinition.MessageBusEventName.GetGameLobbyBalance, function(t, i) {
                        c.isGameLobby = !0;
                        c.model.IsShowBalanceAmount = !1;
                        c.GetGameBalance(i);
                        c.$interval.cancel(c.checkBalanceInterval);
                        c.checkBalanceInterval = c.$interval(c.GetGameBalance.bind(c, i), parseInt(n.GlobalJsConfig.Config.GetGameBalanceTime) * 1e3, 0, !0, null)
                    })
                }
                return t.prototype.GetAllGameBalance = function(t) {
                    var i = this;
                    t.forEach(function(n) {
                        n.Balance = null
                    });
                    this.model.BalanceTotal = this.model.LoginInfo.MainAccountBalance;
                    this.model.CountReturn = 0;
                    t.forEach(function(t) {
                        var r = new n.Models.GetBalancePost;
                        r.GameType = t.GameID;
                        t.Visible === "1" || t.Visible === "0" && t.GameID === "Lover" ? i.loginAreaSvc.GetAllBalance(r).then(function(r) {
                            t.Balance = n.Helpers.RemovePoint(parseFloat(r));
                            t.GameID !== "Lover" && (i.model.BalanceTotal += t.Balance)
                        }).catch(function() {
                            t.Balance = 0
                        }).finally(function() {
                            i.model.CountReturn += 1
                        }) : (t.Balance = 0, i.model.CountReturn += 1)
                    })
                }, t.prototype.GetMemberPlatformAvailableGameList = function() {
                    var t = this;
                    this.loginAreaSvc.GetPlatformServiceInfoAvailableListByAccountID().then(function(n) {
                        t.model.GetPlatformGameAvailableList = n.filter(function(n) {
                            return n.DisplayType === 1 || n.DisplayType === 2 || n.DisplayType === 3
                        });
                        t.model.GetPlatformGameAvailableList.forEach(function(n) {
                            n.Balance = null
                        });
                        t.model.GetPlatformGameAvailableList = n.filter(function(n) {
                            return n.DisplayType === 1 || n.DisplayType === 2
                        });
                        t.model.GetPlatformGameAvailableList = _.sortBy(t.model.GetPlatformGameAvailableList, function(n) {
                            return Number(n.Sort)
                        });
                        t.model.GameMenusBalanceList = _.filter(t.model.GetPlatformGameAvailableList, function(n) {
                            return n.GameID !== "Total" && n.GameID !== "Lover"
                        });
                        t.model.GameMenusBottomList = _.filter(t.model.GetPlatformGameAvailableList, function(n) {
                            return n.GameID === "Total" || n.GameID === "Lover"
                        })
                    }).catch(function(t) {
                        n.Helpers.AlertSwitch(t)
                    })
                }, t.prototype.GetMemberPlatformBalance = function() {
                    var n = this;
                    this.GetBalance(this.model.GetPlatformGameAvailableList, "GetTotal");
                    this.model.IsCanNextGetBalance = !1;
                    this.$timeout(function() {
                        n.model.IsCanNextGetBalance = !0
                    }, 3e4)
                }, t.prototype.InitializeViewModel = function() {
                    this.model = new n.Models.LoginAreaViewModel;
                    this.intervalSwitch = !1;
                    this.isGameLobby = !1;
                    var t = $(".inforMDropT");
                    t.scroll(function() {
                        t.scrollTop() === 0 && t.scrollTop(1)
                    })
                }, t.prototype.RegisterValidation = function() {}, t.prototype.InitializeAdapter = function() {
                    var t = this;
                    this.adapter.Init({
                        HubName: "messageHub",
                        HubUrl: n.GlobalJsConfig.Config.SignalRNFSvcHost,
                        QueryString: "l=1&aid=" + jQuery("#hfAID").val(),
                        IsLogging: n.GlobalJsConfig.Config.SignalRNFSvcIsDebug
                    });
                    this.adapter.Notification.MessageSvcCallback.NotifyMessageAck(function() {
                        t.$timeout(function() {
                            t.GetNotifyMessageUnreadCount()
                        })
                    });
                    this.adapter.Notification.MessageSvcCallback.NotifyMessageUnreadCountAck(function(n) {
                        t.$timeout(function() {
                            t.model.MessageBoxCount = n.Data.TotalItemCount
                        })
                    });
                    this.adapter.Connect().done(function() {
                        t.$timeout(function() {
                            t.GetNotifyMessageUnreadCount()
                        })
                    }).fail(function() {
                        t.model.MessageBoxCount = 0
                    })
                }, t.prototype.GetNotifyMessageUnreadCount = function() {
                    var n = this;
                    this.adapter.Server.MessageSvc.GetNotifyMessageUnreadCount().done(function(t) {
                        n.model.MessageBoxCount = typeof t == "number" ? t : 0
                    }).fail(function() {
                        n.model.MessageBoxCount = 0
                    })
                }, t.prototype.DoSignOut = function() {
                    var t = this;
                    this.blockUI.start();
                    this.loginAreaSvc.SignOut().then(function() {
                        t.adapter.Disconnect();
                        window.obspop && window.obspop.close();
                        location.href = $("#signout").data("signout")
                    }).catch(function() {
                        t.blockUI.stop()
                    });
                    n.Helpers.DeleteLocalStorageItem(n.ConstDefinition.LocalStorageKey.KeepAccountBalance);
                    n.Helpers.DeleteLocalStorageItem(n.ConstDefinition.LocalStorageKey.KeepGameBalance);
                    n.Helpers.DeleteLocalStorageItem(n.ConstDefinition.LocalStorageKey.KeepPointsControlCenter)
                }, t.prototype.GetMainBalance = function() {
                    var n = this;
                    this.loginAreaSvc.GetMemberBalanceInfoByAccountID().then(function(t) {
                        n.model.BalanceAmount = Math.floor(t.BalanceAmount);
                        n.model.LoginInfo.MainAccountBalance = Math.floor(t.BalanceAmount)
                    }).catch(function() {
                        n.model.BalanceAmount = 0
                    }).finally(function() {
                        n.model.IsShowBalanceAmount = !0
                    })
                }, t.prototype.InitializeCountDown = function() {
                    this.intervalSwitch || (this.checkBalanceInterval = this.$interval(this.GetMainBalance.bind(this), parseInt(n.GlobalJsConfig.Config.GetGameBalanceTime) * 1e3, 0, !0, null), this.intervalSwitch = !0)
                }, t.prototype.GetGameBalance = function(t) {
                    var i = this;
                    this.loginAreaSvc.GetGameBalanceInfoByAccountID(t).then(function(t) {
                        i.gameBalance = n.Helpers.RemovePoint(t)
                    }).catch(function() {
                        i.gameBalance = 0
                    }).finally(function() {
                        i.model.IsShowBalanceAmount = !0
                    })
                }, t.prototype.TransferGamesPointToMain = function() {
                    var t = this,
                        i, r;
                    this.blockUI.start();
                    i = $("body");
                    i.css("overflow", "hidden");
                    this.blockUI.done(function() {
                        i.css("overflow", "")
                    });
                    $(".block-ui-container").css("z-index", "9700");
                    this.isTransferGamesPointToMain = !1;
                    r = function() {
                        var n = jQuery(".inforMDrop"),
                            t = $(".inforMDrop,.inforMDropOUT");
                        n.slideUp(200);
                        jQuery(".icon_inforMoney").removeClass("on");
                        t.hide()
                    };
                    this.loginAreaSvc.TransferGamesPointToMain().then(function(t) {
                        n.Helpers.Alert(n.Helpers.ChangeLanguage(t.Message), SweetAlertTypeEnum.success, !1, "", null, function() {
                            n.Helpers.ChangeLanguage(t.Message) !== n.Helpers.ChangeLanguage("您的可轉餘額為0") && r()
                        }, null, null, null, null, !1)
                    }).catch(function(t) {
                        n.Helpers.Alert(n.Helpers.ChangeLanguage(t.Error.Message), SweetAlertTypeEnum.error, !1, "", null, function(n) {
                            n != null && r()
                        }, null, null, null, null, !1)
                    }).finally(function() {
                        t.GetBalance(t.model.GetPlatformGameAvailableList, "GetTotal");
                        t.isTransferGamesPointToMain = !0;
                        t.blockUI.stop()
                    })
                }, t.prototype.GetBalance = function(n, t) {
                    var i = this;
                    this.loginAreaSvc.GetMemberBalanceInfoByAccountID().then(function(r) {
                        i.model.LoginInfo.MainAccountBalance = Math.floor(r.BalanceAmount);
                        t === "GetTotal" && i.GetAllGameBalance(_.filter(n, function(n) {
                            return n.GameID !== "Total"
                        }), r.BalanceAmount)
                    })
                }, t.prototype.ClickSlide = function() {
                    var t = this;
                    this.model.IsCanNextGetBalance && this.loginAreaSvc.GetPlatformServiceInfoAvailableListByAccountID().then(function(n) {
                        t.model.GetPlatformGameAvailableList = n.filter(function(n) {
                            return n.DisplayType === 1 || n.DisplayType === 2 || n.DisplayType === 3
                        });
                        t.model.GetPlatformGameAvailableList.forEach(function(n) {
                            n.Balance = null
                        });
                        t.model.GetPlatformGameAvailableList = n.filter(function(n) {
                            return n.DisplayType === 1 || n.DisplayType === 2
                        });
                        t.model.GetPlatformGameAvailableList = _.sortBy(t.model.GetPlatformGameAvailableList, function(n) {
                            return Number(n.Sort)
                        });
                        t.model.GameMenusBalanceList = _.filter(t.model.GetPlatformGameAvailableList, function(n) {
                            return n.GameID !== "Total" && n.GameID !== "Lover"
                        });
                        t.model.GameMenusBottomList = _.filter(t.model.GetPlatformGameAvailableList, function(n) {
                            return n.GameID === "Total" || n.GameID === "Lover"
                        });
                        t.GetMemberPlatformBalance()
                    }).catch(function(t) {
                        n.Helpers.AlertSwitch(t)
                    })
                }, t.prototype.IsOpenCheck = function() {
                    var n = this.model.LoginStatus !== 2 || !this.model.IsShowBalanceAmount;
                    return !n
                }, t.prototype.CheckTopMenuPermission = function(n) {
                    return this.model.LoginInfo ? this.model.LoginInfo.LoginMenuSwitch[n] === "True" : !1
                }, t.prototype.RedirectPage = function(t) {
                    n.NavigationHelper.GetInstance().RedirectPageToSecondLevel(t)
                }, t.$name = "LoginAreaCtrl", t.$inject = ["$timeout", "$interval", "appConfig", "appContext", "AppContextSvc", "messageBus", "SignalRAdapter", "blockUI", "LoginAreaSvc"], t
            }();
            t.LoginAreaCtrl = i
        })(t = n.Controllers || (n.Controllers = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterController(OBSMobileApp.Controllers.LoginAreaCtrl.$name, OBSMobileApp.Controllers.LoginAreaCtrl),
    function(n) {
        var t;
        (function(n) {
            var t = function() {
                function n() {
                    this.FingerIDX = "";
                    this.ShowSliderCaptcha = !1;
                    this.VerifySliderCaptcha = !1;
                    this.CellPhone = "";
                    this.IsCellPhoneValid = !1;
                    this.IdyKey = ""
                }
                return n
            }();
            n.LoginPopupModel = t
        })(t = n.Models || (n.Models = {}))
    }(OBSMobileApp || (OBSMobileApp = {})),
    function(n) {
        var t;
        (function(n) {
            var t = function() {
                function n(n) {
                    this.dataProvider = n
                }
                return n.prototype.SignIn = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Authorize/SignIn", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n.Data)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.SignCheck = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Authorize/SignCheck", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.GetDBAConfigMemberPaymemtBySettingType = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("/api/Common/GetDBAConfigMemberPaymemtBySettingType", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.$name = "LoginPopupSvc", n.$inject = ["DataProvider"], n
            }();
            n.LoginPopupSvc = t
        })(t = n.Services || (n.Services = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterService(OBSMobileApp.Services.LoginPopupSvc.$name, OBSMobileApp.Services.LoginPopupSvc),
    function(n) {
        var t;
        (function(t) {
            var i = function() {
                function t(n, t, i, r, u, f, e) {
                    var o = this;
                    this.$scope = n;
                    this.$timeout = t;
                    this.loginPopupSvc = i;
                    this.permissionSvc = r;
                    this.blockUI = u;
                    this.toolsSvc = f;
                    this.messageBus = e;
                    this.debounceSignInLoad = _.debounce(function() {
                        return o.DoSignIn()
                    }, 500, {
                        leading: !0,
                        trailing: !1
                    });
                    this.InitializeViewModel()
                }
                return t.prototype.InitializeViewModel = function() {
                    this.login = new n.Models.LoginPopupModel;
                    jQuery().ready(function() {
                        document.addEventListener("touchmove", function() {
                            $(":text,:password").trigger("blur")
                        }, !1)
                    })
                }, t.prototype.RedirectNextCase = function() {
                    var u = this,
                        r = (new Date).getTime(),
                        t = location.pathname,
                        i;
                    switch (t.toLowerCase()) {
                        case "/mobile/game/boardgamen":
                        case "/mobile/game/electgamen":
                        case "/mobile/game/livegamen":
                        case "/mobile/game/lotterygamen":
                        case "/mobile/game/sportgamen":
                            t = t.substring(0, t.length - 1);
                            location.href = t;
                            break;
                        default:
                            i = navigator.userAgent.toLowerCase();
                            location.href = i.indexOf("ucbrowser") > -1 || i.indexOf("micromessenger") > -1 ? "/Mobile/Home/Index?" + r : "/Mobile/Home/Index"
                    }
                    /OppoBrowser/i.test(window.navigator.userAgent) && this.$timeout(function() {
                        var t = (new Date).getTime();
                        t - 2500 > r ? window.location.reload() : (u.blockUI.stop(), $("#btnSignIn").attr("value", n.Helpers.ChangeLanguage("登 錄")), $("#btnSignIn").prop("disabled", !1), $("#btnSignIn").removeClass("btn-disabled"))
                    }, 2e3)
                }, t.prototype.RegisterValidation = function() {
                    jQuery.validator.addMethod("ckAccountID", function(t) {
                        return n.Validator.IsAccountIDFormatValid(t)
                    });
                    jQuery.validator.addMethod("ckSafetyAccountID", function(t) {
                        return n.Validator.IsAccountIDFormatValid(t)
                    });
                    jQuery.validator.addMethod("ckAccountPWD", function(t) {
                        return n.Validator.IsPasswordFormatValid(t)
                    });
                    jQuery.validator.addMethod("ckCellPhoneVerifyByLength", function(t) {
                        return n.Validator.IsCellPhoneByLengthFormatValid(t)
                    });
                    jQuery.validator.addMethod("ckCellPhoneVerify", function(t) {
                        return n.Validator.IsCellPhoneFormatValid(t)
                    })
                }, t.prototype.ClearForm = function() {
                    this.login.AccountID = "";
                    this.login.AccountPWD = "";
                    this.login.CellPhone = "";
                    this.messageBus.Emit("OnSliderCaptchaReset", {});
                    jQuery("#frmLogin .error_login_t").remove();
                    jQuery("#frmLogin .login_list").removeClass("error_login")
                }, t.prototype.DoSignIn = function() {
                    var t = this,
                        i;
                    this.blockUI.start();
                    i = angular.copy(this.login);
                    i.AccountPWD = this.toolsSvc.Base64Encode(i.AccountPWD);
                    i.LocalStorgeCookie = n.Helpers.GetLocalStorageItem(n.ConstDefinition.LocalStorageKey.IT);
                    i.ScreenResolution = screen.width + "*" + screen.height;
                    Fingerprint2.get(function(r) {
                        i.FingerIDX = Fingerprint2.x64hash128(r.map(function(n) {
                            return n.value
                        }).join(), 31);
                        t.loginPopupSvc.SignIn(i).then(function(i) {
                            n.Helpers.SetLocalStorageItem(n.ConstDefinition.LocalStorageKey.IT, i.CookieID, !1);
                            n.Helpers.SetLocalStorageItem(n.ConstDefinition.LocalStorageKey.LatestTimeForDepositNews, "", !0);
                            n.Helpers.SetLocalStorageItem(n.ConstDefinition.LocalStorageKey.LatestTimeForWithdrawalNews, "", !0);
                            n.Helpers.DeleteLocalStorageItem(n.ConstDefinition.LocalStorageKey.KeepAccountBalance);
                            n.Helpers.DeleteLocalStorageItem(n.ConstDefinition.LocalStorageKey.KeepGameBalance);
                            n.Helpers.DeleteLocalStorageItem(n.ConstDefinition.LocalStorageKey.KeepPointsControlCenter);
                            t.RedirectNextCase()
                        }).catch(function(i) {
                            if (i.Error.Code == ApiStatusCodeEnum.PermissionDenied) {
                                location.href = "/Mobile/Error/Restricted";
                                return
                            }(i.Error.Code == ApiStatusCodeEnum.OpenSliderCaptcha || i.Error.Code == ApiStatusCodeEnum.CellPhoneEmpty) && (t.login.ShowSliderCaptcha = !0, jQuery.fancybox.update());
                            t.login.ShowSliderCaptcha && (t.messageBus.Emit("OnSliderCaptchaReset", {}), t.login.VerifySliderCaptcha = !1, t.login.AccountID = "");
                            t.blockUI.stop();
                            $("#btnSignIn").attr("value", n.Helpers.ChangeLanguage("登 錄"));
                            $("#btnSignIn").prop("disabled", !1);
                            $("#btnSignIn").removeClass("btn-disabled");
                            i.Error.Code != ApiStatusCodeEnum.CellPhoneEmpty && n.Helpers.Alert(i.Error.Message, SweetAlertTypeEnum.error);
                            t.login.CellPhone = "";
                            t.login.AccountPWD = ""
                        })
                    })
                }, t.prototype.SignCheck = function() {
                    $("#frmLogin").valid() && !this.CheckDisabled() && ($("#btnSignIn").attr("value", n.Helpers.ChangeLanguage("登錄中")), $("#btnSignIn").attr("disabled", "disabled"), $("#btnSignIn").addClass("btn-disabled"), this.debounceSignInLoad())
                }, t.prototype.CheckIfRegister = function() {
                    this.permissionSvc.IsMemberRegisterEnabled().then(function(t) {
                        if (t === !0) {
                            window.location.href = jQuery("#registerurl").val();
                            return
                        }
                        n.Helpers.Alert("很抱歉，目前會員註冊關閉中", SweetAlertTypeEnum.warning, !1, "", null, function() {
                            window.location.href = "/Mobile/Home/Index"
                        })
                    }).catch(function(t) {
                        n.Helpers.Alert(t.Error.Message, SweetAlertTypeEnum.error)
                    })
                }, t.prototype.TriggerInputOnBlur = function(n) {
                    return n != null ? n.replace(/\s/g, "") : ""
                }, t.prototype.VerifyCallback = function(n) {
                    var t = this;
                    this.$timeout(function() {
                        t.login.VerifySliderCaptcha = n
                    })
                }, t.prototype.CheckDisabled = function() {
                    return this.login.ShowSliderCaptcha ? !this.login.AccountID || !this.login.AccountPWD || !this.login.IsCellPhoneValid || !this.login.VerifySliderCaptcha : !this.login.AccountID || !this.login.AccountPWD
                }, t.prototype.VerifyCellPhoneByJqueryValid = function() {
                    $("#hfCellPhone").valid()
                }, t.prototype.VerifyCellPhone = function() {
                    this.login.IsCellPhoneValid = n.Validator.IsCellPhoneFormatValid(this.login.CellPhone);
                    this.login.IsCellPhoneValid && this.VerifyCellPhoneByJqueryValid()
                }, t.prototype.KeyboardClose = function() {
                    this.messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnMobileKeyboardClose, {
                        elemId: "CellPhone"
                    })
                }, t.prototype.VerifyAccountID = function() {
                    this.login.ShowSliderCaptcha && $("#txtAccountID").valid()
                }, t.prototype.VerifyAccountPWD = function() {
                    this.login.ShowSliderCaptcha && $("#txtAccountPWD").valid()
                }, t.$name = "LoginPopupCtrl", t.$inject = ["$scope", "$timeout", "LoginPopupSvc", "PermissionSvc", "blockUI", "ToolsSvc", "messageBus"], t
            }();
            t.LoginPopupCtrl = i
        })(t = n.Controllers || (n.Controllers = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterController(OBSMobileApp.Controllers.LoginPopupCtrl.$name, OBSMobileApp.Controllers.LoginPopupCtrl),
    function(n) {
        var t;
        (function(n) {
            var t = function() {
                function n() {}
                return n
            }();
            n.MaskJoinInfoNewViewModel = t
        })(t = n.Models || (n.Models = {}))
    }(OBSMobileApp || (OBSMobileApp = {})),
    function(n) {
        var t;
        (function(n) {
            var t = function() {
                function n(n, t, i, r) {
                    this.adapter = n;
                    this.dataProvider = t;
                    this.$interval = i;
                    this.appConfig = r;
                    this.intervalSwitch = !1
                }
                return n.prototype.RegisterValidation = function() {}, n.prototype.FancyBoxClose = function() {
                    jQuery.fancybox.close()
                }, n.$name = "MaskJoinInfoNewCtrl", n.$inject = ["SignalRAdapter", "DataProvider", "$interval", "appConfig"], n
            }();
            n.MaskJoinInfoNewCtrl = t
        })(t = n.Controllers || (n.Controllers = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterController(OBSMobileApp.Controllers.MaskJoinInfoNewCtrl.$name, OBSMobileApp.Controllers.MaskJoinInfoNewCtrl),
    function(n) {
        var t;
        (function(n) {
            var i = function() {
                    function t() {
                        this.PointsControlCenter = new n.GetPointsControlCenter;
                        this.MonitorGetGameBalanceFinishTime = 500;
                        this.KeepFinishReloadBalanceTime = 1e4
                    }
                    return t
                }(),
                t;
            n.PointsControlCenterViewModel = i;
            t = function() {
                function n() {}
                return n
            }();
            n.GetBalancePostModel = t
        })(t = n.Models || (n.Models = {}))
    }(OBSMobileApp || (OBSMobileApp = {})),
    function(n) {
        var t;
        (function(n) {
            var t = function() {
                function n(n) {
                    this.dataProvider = n
                }
                return n.prototype.GetMemberBalanceInfoByAccountID = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/MemberTransfer/GetMemberBalanceInfoByAccountID", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.SignCheck = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Authorize/SignCheck", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.prototype.GetGameBalanceByGameType = function(n) {
                    var t = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Game/GetBalance", HttpMethodEnum.Post, n).then(function(n) {
                        t.resolve(n.Data)
                    }).catch(function(n) {
                        t.reject(n)
                    }), t.promise
                }, n.prototype.GetAliveGameList = function() {
                    var n = this.dataProvider.CreateDeferred();
                    return this.dataProvider.Get("../../api/Game/GetAliveGameList", HttpMethodEnum.Post).then(function(t) {
                        n.resolve(t.Data)
                    }).catch(function(t) {
                        n.reject(t)
                    }), n.promise
                }, n.$name = "PointsControlCenterSvc", n.$inject = ["DataProvider"], n
            }();
            n.PointsControlCenterSvc = t
        })(t = n.Services || (n.Services = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterService(OBSMobileApp.Services.PointsControlCenterSvc.$name, OBSMobileApp.Services.PointsControlCenterSvc),
    function(n) {
        var t;
        (function(t) {
            var i = function() {
                function t(n, t, i, r, u, f) {
                    this.mainAccountPointSvc = n;
                    this.$interval = t;
                    this.$messageBus = i;
                    this.appConfig = r;
                    this.blockUI = u;
                    this.$timeout = f;
                    this.InitializeViewModel()
                }
                return t.prototype.InitializeViewModel = function() {
                    var t = this,
                        i, r;
                    this.model = new n.Models.PointsControlCenterViewModel;
                    this.$messageBus.On(n.ConstDefinition.MessageBusEventName.OnRefreshAllPointsControlCenter, function() {
                        t.AllReload()
                    });
                    this.$messageBus.On(n.ConstDefinition.MessageBusEventName.OnSetOneGameReLoadPointsControlCenter, function(n, i) {
                        t.GetOneGameBalance(i)
                    });
                    i = n.Helpers.GetLocalStorageItem(n.ConstDefinition.LocalStorageKey.KeepPointsControlCenter);
                    r = !0;
                    i != null && i !== "" && (angular.copy(JSON.parse(i), this.model.PointsControlCenter), this.model.PointsControlCenter.IsGetGameBalance = !0, this.model.PointsControlCenter.CountReturn = this.model.PointsControlCenter.GameAvailableList.length + 1, this.$timeout(function() {
                        t.$messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnGetPointsControlCenter, t.model.PointsControlCenter)
                    }), this.$timeout(function() {
                        t.GetMemberPlatformAvailableGameList()
                    }, this.model.KeepFinishReloadBalanceTime), r = !1);
                    this.SignCheck(r)
                }, t.prototype.RegisterValidation = function() {}, t.prototype.SignCheck = function(n) {
                    var t = this;
                    n === void 0 && (n = !0);
                    this.mainAccountPointSvc.SignCheck().then(function(i) {
                        if (i === !1) {
                            t.model.PointsControlCenter.IsSignCheck = !1;
                            return
                        }
                        n && t.GetMemberPlatformAvailableGameListForInit()
                    }).catch(function() {
                        angular.isDefined(t.checkAccountBalanceInterval) && (t.$interval.cancel(t.checkAccountBalanceInterval), t.checkAccountBalanceInterval = null)
                    });
                    this.StartIntervalGetAccountBalance()
                }, t.prototype.GetMemberPlatformAvailableGameListForInit = function() {
                    var n = jQuery("#hfAvailablePlatform").val();
                    jQuery("#hfAvailablePlatform").val("");
                    n != undefined && n !== "" ? (this.model.PointsControlCenter.GameAvailableList = JSON.parse(n), this.model.PointsControlCenter.SetAllReload(), this.GetBalance(GetBalanceActionEnum.MainAccountAndAllGameBalance)) : this.GetMemberPlatformAvailableGameList()
                }, t.prototype.GetMemberPlatformAvailableGameList = function() {
                    var t = this;
                    if (!this.model.PointsControlCenter.IsGetGameBalance) {
                        this.GetBalance(GetBalanceActionEnum.MainAccountBalance);
                        return
                    }
                    this.mainAccountPointSvc.GetAliveGameList().then(function(n) {
                        n = n.filter(function(n) {
                            return (n.DisplayType === 1 || n.DisplayType === 2 || n.DisplayType === 3) && n.ServiceID !== "Member"
                        });
                        t.model.PointsControlCenter.GameAvailableList = n;
                        t.model.PointsControlCenter.SetAllReload();
                        t.GetBalance(GetBalanceActionEnum.MainAccountAndAllGameBalance)
                    }).catch(function(t) {
                        n.Helpers.AlertSwitch(t)
                    })
                }, t.prototype.GetBalance = function(t) {
                    var i = this;
                    this.$messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnGetPointsControlCenter, this.model.PointsControlCenter);
                    this.mainAccountPointSvc.GetMemberBalanceInfoByAccountID().then(function(r) {
                        if (i.model.PointsControlCenter.AccountBalance = Math.floor(r.BalanceAmount), i.$messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnGetPointsControlCenter, i.model.PointsControlCenter), i.model.PointsControlCenter.IsGetGameBalance == !0 && t === GetBalanceActionEnum.MainAccountAndAllGameBalance) i.$timeout(function() {
                            i.model.PointsControlCenter.IsGetGameBalance = !0
                        }, 1e4), i.model.PointsControlCenter.IsGetGameBalance = !1, i.$timeout.cancel(i.gameBalanceTimer), i.gameBalanceTimer = i.$timeout(function() {
                            i.GetGameBalance();
                            i.$timeout.cancel(i.gameBalanceFinishTimer);
                            i.gameBalanceFinishTimer = i.$timeout(function() {
                                i.CheckGetGameBalanceFinish()
                            }, i.model.MonitorGetGameBalanceFinishTime)
                        }, 1e3);
                        else {
                            var u = i.model.PointsControlCenter.GameAvailableList.filter(function(n) {
                                return n.Balance > 0
                            }).length > 0;
                            u && i.model.PointsControlCenter.AccountBalance == i.model.PointsControlCenter.TotalBalance && i.AllReload()
                        }
                    }).catch(function() {
                        i.model.PointsControlCenter.AccountBalance = 0
                    }).finally(function() {})
                }, t.prototype.CheckGetGameBalanceFinish = function() {
                    var r = this,
                        t = _.filter(this.model.PointsControlCenter.GameAvailableList, function(n) {
                            return n.IsBalanceLoading == !1
                        }).length + 1,
                        i = this.model.PointsControlCenter.CountReturn !== t;
                    this.model.PointsControlCenter.CountReturn = t;
                    this.model.PointsControlCenter.CountReturn <= this.model.PointsControlCenter.GameAvailableList.length ? this.$timeout(function() {
                        r.CheckGetGameBalanceFinish()
                    }, this.model.MonitorGetGameBalanceFinishTime) : i = !0;
                    i && (this.SetMemberGameBalance(), this.$messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnGetPointsControlCenter, this.model.PointsControlCenter), n.Helpers.SetLocalStorageItem(n.ConstDefinition.LocalStorageKey.KeepPointsControlCenter, angular.toJson(this.model.PointsControlCenter), !0))
                }, t.prototype.GetGameBalance = function() {
                    var t = this;
                    this.model.PointsControlCenter.CountReturn = 1;
                    this.model.PointsControlCenter.GameAvailableList.forEach(function(i) {
                        if (i.Balance == null && !i.IsBalanceLoading) {
                            var r = new n.Models.GetBalancePostModel;
                            r.GameType = i.GameID;
                            i.Visible === "1" ? (i.IsBalanceLoading = !0, t.mainAccountPointSvc.GetGameBalanceByGameType(r).then(function(t) {
                                i.Balance = n.Helpers.RemovePoint(parseFloat(t))
                            }).catch(function() {
                                i.Balance = 0
                            }).finally(function() {
                                i.IsBalanceLoading = !1
                            })) : i.Balance = 0
                        }
                    })
                }, t.prototype.GetOneGameBalance = function(t) {
                    var i = this;
                    t != "" && t != null && (this.model.PointsControlCenter.CountReturn = 1, this.model.PointsControlCenter.SetGameReload(t), this.$messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnGetPointsControlCenter, this.model.PointsControlCenter), this.mainAccountPointSvc.GetMemberBalanceInfoByAccountID().then(function(r) {
                        i.model.PointsControlCenter.AccountBalance = Math.floor(r.BalanceAmount);
                        i.$messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnGetPointsControlCenter, i.model.PointsControlCenter);
                        i.model.PointsControlCenter.GameAvailableList.filter(function(n) {
                            return n.GameID == t
                        }).forEach(function(t) {
                            if (t.Balance == null && !t.IsBalanceLoading) {
                                var r = new n.Models.GetBalancePostModel;
                                r.GameType = t.GameID;
                                t.IsBalanceLoading = !0;
                                i.mainAccountPointSvc.GetGameBalanceByGameType(r).then(function(i) {
                                    t.Balance = n.Helpers.RemovePoint(parseFloat(i))
                                }).catch(function() {
                                    t.Balance = 0
                                }).finally(function() {
                                    t.IsBalanceLoading = !1;
                                    i.model.PointsControlCenter.CountReturn = _.filter(i.model.PointsControlCenter.GameAvailableList, function(n) {
                                        return n.IsBalanceLoading == !1
                                    }).length + 1;
                                    i.SetMemberGameBalance();
                                    n.Helpers.SetLocalStorageItem(n.ConstDefinition.LocalStorageKey.KeepPointsControlCenter, angular.toJson(i.model.PointsControlCenter), !0);
                                    i.$messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnGetPointsControlCenter, i.model.PointsControlCenter)
                                })
                            }
                        })
                    }).catch(function() {
                        i.model.PointsControlCenter.AccountBalance = 0
                    }).finally(function() {
                        n.Helpers.SetLocalStorageItem(n.ConstDefinition.LocalStorageKey.KeepPointsControlCenter, angular.toJson(i.model.PointsControlCenter), !0);
                        i.$messageBus.Emit(n.ConstDefinition.MessageBusEventName.OnGetPointsControlCenter, i.model.PointsControlCenter)
                    }))
                }, t.prototype.SetMemberGameBalance = function() {
                    var n = this.model.PointsControlCenter.AccountBalance;
                    this.model.PointsControlCenter.GameAvailableList.forEach(function(t) {
                        t.Visible === "1" ? t.Sort < 100 && (n += t.Balance) : t.Balance = 0
                    });
                    this.model.PointsControlCenter.TotalBalance = n
                }, t.prototype.AllReload = function() {
                    this.GetMemberPlatformAvailableGameList()
                }, t.prototype.StartIntervalGetAccountBalance = function() {
                    (angular.isDefined(this.checkAccountBalanceInterval) || this.checkAccountBalanceInterval !== null) && (this.$interval.cancel(this.checkAccountBalanceInterval), this.checkAccountBalanceInterval = null);
                    this.checkAccountBalanceInterval = this.$interval(this.GetBalance.bind(this), parseInt(n.GlobalJsConfig.Config.GetGameBalanceTime) * 1e3, 0, !0, null, "interval")
                }, t.$name = "PointsControlCenterCtrl", t.$inject = ["PointsControlCenterSvc", "$interval", "messageBus", "appConfig", "blockUI", "$timeout"], t
            }();
            t.PointsControlCenterCtrl = i
        })(t = n.Controllers || (n.Controllers = {}))
    }(OBSMobileApp || (OBSMobileApp = {}));
OBSMobileApp.RegisterAngular.RegisterController(OBSMobileApp.Controllers.PointsControlCenterCtrl.$name, OBSMobileApp.Controllers.PointsControlCenterCtrl)
