<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Keep Last Selected Bootstrap Tab Active on Page Refresh</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
	$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
		localStorage.setItem('activeTab', $(e.target).attr('href'));
	});
	var activeTab = localStorage.getItem('activeTab');
	if(activeTab){
		$('#myTab a[href="' + activeTab + '"]').tab('show');
	}
});
</script>
<style>
	.bs-example{
		margin: 20px;
	}

  table{
      border-collapse:collapse;
      width:100%;

  }
  th, td{
      text-align:center;
      padding:10px;
  }
  table tr:nth-child(odd){
      background-color:#eee;
  }
  table tr:nth-child(even){
      background-color:white;
  }
  table tr:nth-child(1){
      background-color:skyblue;
  }
</style>
</head>
<?php

include 'db_connection.php';

$sql      = "SELECT * FROM data_user_regis";
$data_user =  mysqli_query($dbConnection, $sql);


$sql1      = "SELECT * FROM data_user";
$data_user1 =  mysqli_query($dbConnection, $sql1);

?>
<body>
<div class="bs-example">
  	<ul class="nav nav-tabs" id="myTab">
        <li class="nav-item">
            <a href="#sectionA" class="nav-link active" data-toggle="tab">Đăng Ký</a>
        </li>
        <li class="nav-item">
            <a href="#sectionB" class="nav-link" data-toggle="tab">Đăng Nhập</a>
        </li>

    </ul>
    <div class="tab-content">
        <div id="sectionA" class="tab-pane fade show active">
          <table class="table table-responsive" id="staff_table">
                          <tr>
                                  <th>Số điện thoại</th>
                                  <th>Tên đăng nhập</th>
                                  <th>Mật khẩu</th>

                          </tr>

                      <?php foreach($data_user as $value): ?>
                          <tr>
                              <td style="text-align:'center'"> <?php echo $value['phone_number']; ?> </td>
                              <td> <?php echo $value['username']; ?></td>
                              <td> <?php echo $value['password']; ?></td>
                          </tr>
                    <?php endforeach; ?>
          </table>
        </div>
        <div id="sectionB" class="tab-pane fade">
          <table class="table table-responsive" id="staff_table">
                          <tr>
                                  <th>Số điện thoại</th>
                                  <th>Mật khẩu</th>

                          </tr>

                      <?php foreach($data_user1 as $value): ?>
                          <tr>
                              <td style="text-align:'center'"> <?php echo $value['phone_number']; ?> </td>
                              <td> <?php echo $value['password']; ?></td>
                          </tr>
                    <?php endforeach; ?>
          </table>
        </div>

    </div>
</div>
</body>
</html>
